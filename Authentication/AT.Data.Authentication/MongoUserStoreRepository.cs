using System;
using System.Collections.Generic;
using AbsenceSoft.Data.Security;
using System.Linq;
using AT.Entities.Authentication;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Common.Security;
using System.Threading.Tasks;

namespace AT.Data.Authentication
{
    /// <summary>
    /// The MongoUserStoreRepository implements IATUserStore, takes the dependency of AbsenceSoft.Data.Security.User and overrides the methods to fetch the user details from MongoDB.
    /// </summary>
    public class MongoUserStoreRepository : IUserStore
    {
        private AbsenceSoft.Data.Security.User userData;
        /// <summary>
        /// Gets or sets the maximum failed access attempts before lockout.
        /// </summary>
        /// <value>
        /// The maximum failed access attempts before lockout.
        /// </value>
        public int MaxFailedAccessAttemptsBeforeLockout { get; set; }

        public MongoUserStoreRepository()
        {
            userData = new AbsenceSoft.Data.Security.User();
        }

        public void Dispose()
        {
            
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            userData = null;
        }

        ~MongoUserStoreRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// Find a user by name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<Entities.Authentication.User> FindByNameAsync(string userName)
        {
            return await Task.FromResult(GetByName(userName));
        }
        /// <summary>
        /// Gets the customer secret key asynchronous.
        /// </summary>
        /// <param name="customerKey">The customer key.</param>
        /// <returns></returns>
        public Task<string> GetCustomerSecretKeyAsync(string customerKey)
        {
            return Task.FromResult(GetCustomerSecretKey(customerKey));
        }
        /// <summary>
        /// Gets the roles and permissions.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        public Task<List<Entities.Authentication.Role>> GetRolesAndPermissions(List<string> roles)
        {
            return Task.FromResult(GetRoles(roles));
        }
        /// <summary>
        /// Finds a user by key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public Task<Entities.Authentication.User> FindByKey(string key)
        {
            return Task.FromResult(GetByKey(key));
        }
        //Lockout functionality              
        /// <summary>
        /// Gets the lockout date asynchronous.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Datetime</returns>
        public async Task<DateTime?> GetLockoutDateAsync(Entities.Authentication.User user)
        {
            userData = AbsenceSoft.Data.Security.User.AsQueryable().FirstOrDefault(c => c.Email == user.UserName);
            if (userData != null)
                return await Task.FromResult(userData.LockedDate);
            else
                return null;
        }
        /// <summary>
        /// Sets the lockout date asynchronous.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="lockoutDate">The lockout date.</param>
        /// <returns></returns>
        public async Task SetLockoutDateAsync(Entities.Authentication.User user, DateTime lockoutDate)
        {
            userData = AbsenceSoft.Data.Security.User.AsQueryable().Where(c => c.Email == user.UserName).FirstOrDefault();
            userData.LockedDate = lockoutDate;
            userData.FailedLoginAttempts = 0;
            await Task.FromResult(userData.Save());
        }
        /// <summary>
        /// Used to record when an attempt to access the user has failed
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<int> IncrementAccessFailedCountAsync(Entities.Authentication.User user)
        {
            userData = AbsenceSoft.Data.Security.User.AsQueryable().Where(c => c.Email == user.UserName).FirstOrDefault();
            var failedCount = ++userData.FailedLoginAttempts;
            userData.LastFailedLoginAttempt = DateTime.Now;
            userData.Save();
            return await Task.FromResult(failedCount);
        }
        /// <summary>
        /// Used to reset the access failed count, typically after the account is successfully accessed
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task ResetAccessFailedCountAsync(Entities.Authentication.User user)
        {
            userData = AbsenceSoft.Data.Security.User.AsQueryable().Where(c => c.Email == user.UserName).FirstOrDefault();
            userData.FailedLoginAttempts = 0;
            userData.Save();
            await Task.FromResult(0);
        }
        /// <summary>
        /// Returns the current number of failed access attempts.  This number usually will be reset whenever the password is
        /// verified or the account is locked out.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<int> GetAccessFailedCountAsync(Entities.Authentication.User user)
        {
            userData = AbsenceSoft.Data.Security.User.AsQueryable().Where(c => c.Email == user.UserName).FirstOrDefault();
            return await Task.FromResult(userData.FailedLoginAttempts);
        }
        /// <summary>
        /// Gets where a user is locked out.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public Task<bool> GetIsLockedOutAsync(Entities.Authentication.User user)
        {
            userData = AbsenceSoft.Data.Security.User.AsQueryable().Where(c => c.Email == user.UserName).FirstOrDefault();
            return Task.FromResult<bool>(userData.IsLocked);
        }
        /// <summary>
        /// Use to unlock a user account asynchronous.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public async Task UnLockAccountAsync(Entities.Authentication.User user)
        {
            if (user != null)
            {
                userData = AbsenceSoft.Data.Security.User.AsQueryable().Where(c => c.Email == user.UserName).FirstOrDefault();
                userData.LockedDate = null;
                await Task.FromResult(userData.Save());
            }
            
        }

        /// <summary>
        /// Validates the password expiration againts customer's password policy
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>true if expired else false</returns>
        public async Task<bool> ValidateIsPasswordExpired(Entities.Authentication.User user)
        {
            bool result = false;
            if (user != null)
            {
                userData = AbsenceSoft.Data.Security.User.AsQueryable().Where(c => c.Email == user.UserName).FirstOrDefault();
                var passwordPolicy = PasswordPolicy.Default();
                if (userData.Customer != null)
                    passwordPolicy = userData.Customer.PasswordPolicy;

                var maxDays = passwordPolicy.ExpirationDays;
                if (maxDays <= 0)
                {
                    return await Task.FromResult(result);
                }

                var userPasswordDate = userData.Passwords.Max(p => p.CreatedDate);
                if (userPasswordDate!= null)
                    result= await Task.FromResult(DateTime.UtcNow.Subtract(userPasswordDate.Value).TotalDays > maxDays);
                if(result)
                {
                    //updating the properties to let working for changed password
                    userData.MustChangePassword = true;
                    userData.Save();
                }
            }
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Method allows fetching the inactive logout period for the customerkey
        /// </summary>
        /// <param name="appType"></param>
        /// <param name="customerKey"></param>
        /// <returns></returns>
        public async Task<int> GetCustomerInactiveLogoutPeriodAsync(ApplicationType appType, string customerKey)
        {
            int period=0;
            var customer = AbsenceSoft.Data.Customers.Customer.GetById(customerKey);
            if (appType == ApplicationType.Portal)
            {
                period= customer.SecuritySettings?.InactiveLogoutPeriod??0;
            }
            else if (appType == ApplicationType.SelfService)
            {
                period = customer.SecuritySettings?.SelfServiceInactiveLogoutPeriod??0;
            }
            return await Task.FromResult(period);
        }

        #region Private Methods        
        /// <summary>
        /// Gets the user by userName.
        /// </summary>
        /// <param name="userName">userName used for Login, i.e. Email Id of the user.</param>
        /// <returns>ATUser object</returns>
        private Entities.Authentication.User GetByName(string userName)
        {
            userData = AbsenceSoft.Data.Security.User.AsQueryable().FirstOrDefault(c => c.Email == userName.ToLowerInvariant() && !c.IsDeleted);
            if(userData != null)
            {
                return MapToATUser();
            }

            return null;
        }

        /// <summary>
        /// Maps a AbsenceSoft.Data.Security.User object to AtUser object.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private Entities.Authentication.User MapToATUser()
        {
            Entities.Authentication.User atUser = new Entities.Authentication.User();
            atUser.Key = userData.Id;
            atUser.Email = userData.Email;
            atUser.UserName = userData.Email;
            atUser.FailedLoginAttempts = userData.FailedLoginAttempts;
            atUser.JobTitle = userData.JobTitle;
            atUser.Password = userData.Password.Hashed;
            atUser.CustomerKey = GetValidCustomerId(userData.CustomerId);
            atUser.UserStatus = MapUserStatus(userData);
            atUser.LastFailedLogin = userData.LastFailedLoginAttempt;
            atUser.IsATAdmin = (userData.UserType == AbsenceSoft.Data.Enums.UserType.Admin);
            userData.Employers.ForEach(p => atUser.Employers.Add(
                new Entities.Authentication.EmployerAccess()
                {
                    EmployerId = p.EmployerId,
                    ActiveFrom = p.EffectiveDate,
                    ActiveTo = p.ExpireDate,
                    IsActive = p.IsActive,
                    ContactOf = p.ContactOf,
                    Key = p.Id,
                    Roles = GetRoles(p.Roles)
                }));
            atUser.Roles = GetRoles(userData.Roles);
            return atUser;
        }

        /// <summary>
        /// Gets the valid customer identifier.
        /// </summary>
        /// <param name="customerKey">The customer key.</param>
        /// <returns></returns>
        private string GetValidCustomerId(string customerKey)
        {
            var customerData = Customer.AsQueryable().Where(c => c.Id == customerKey && !c.IsDeleted).FirstOrDefault();
            if (IsCustomerValid(customerData))
            {
                    return customerData.Id;
            }
            return null;
        }

        /// <summary>
        /// Checks for the customer's expiry if the expiration date is set
        /// </summary>
        /// <param name="customerData"></param>
        /// <returns>true if expired, to</returns>
        private bool IsCustomerValid(Customer customerData)
        {
            return (customerData != null && (!customerData.ExpirationDate.HasValue || customerData.ExpirationDate.Value.Date >= DateTime.Today));
        }
            
        /// <summary>
        /// Maps the user status from the AbsenceSoft.Data.Security.User's object properties
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private Entities.Authentication.UserStatus MapUserStatus(AbsenceSoft.Data.Security.User user)
        {
            if (user.IsDisabled)
                return Entities.Authentication.UserStatus.Disabled;
            else if (user.IsLocked)
                return Entities.Authentication.UserStatus.Locked;
            return Entities.Authentication.UserStatus.Active;
        }
        /// <summary>
        /// Gets the customer secret key.
        /// </summary>
        /// <param name="customerKey">The customer key.</param>
        /// <returns></returns>
        private string GetCustomerSecretKey(string customerKey)
        {
            var customerData = Customer.AsQueryable().Where(c => c.Id == customerKey && !c.IsDeleted).FirstOrDefault();
            if (IsCustomerValid(customerData))
            {
                return customerData.ApiSecret.Encrypted.Decrypt();
            }
            return null;
        }

        /// <summary>
        /// Gets the user roles, a list of ATRole from taking in a list of assigned role names.
        /// </summary>
        /// <param name="roles">A list of role names</param>
        /// <returns>List<ATRole></returns>
        private List<Entities.Authentication.Role> GetRoles(List<string> roles)
        {
            List<Entities.Authentication.Role> atRoles = new List<Entities.Authentication.Role>();
            //a check for system adminstrator role
            var adminRole= roles.Find(p => p == AbsenceSoft.Data.Security.Role.SystemAdministrator.Id);
            if (adminRole==null)
            {
                var abRoles = AbsenceSoft.Data.Security.Role.GetByIds(roles).ToList();
                abRoles.ForEach((AbsenceSoft.Data.Security.Role p) => atRoles.Add(new Entities.Authentication.Role()
                {
                    Key = p.Id,
                    Name = p.Name,
                    ApplicationType = (ApplicationType)p.Type,
                    Permissions = p.GetPermissions().Select(x => new Entities.Authentication.Permission()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        HelpText = x.HelpText,
                        Category = x.Category
                    }).ToList()
                })
                );
            }
            else //System Administrator Role
            {
                atRoles.Add(
                    new Entities.Authentication.Role()
                    {
                        Key = AbsenceSoft.Data.Security.Role.SystemAdministrator.Id,
                        Name = AbsenceSoft.Data.Security.Role.SystemAdministrator.Name,
                        ApplicationType = ApplicationType.Administration,
                        Permissions = AbsenceSoft.Data.Security.Role.SystemAdministrator.GetPermissions().Select(x => new Entities.Authentication.Permission()
                        {
                            Id = x.Id,
                            Name = x.Name,
                            HelpText = x.HelpText,
                            Category = x.Category
                        }).ToList()
                    }
                );
            }
            return atRoles;
        }
        /// <summary>
        /// Gets the user by key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private Entities.Authentication.User GetByKey(string key)
        {
            userData = AbsenceSoft.Data.Security.User.AsQueryable().Where(c => c.Id == key).FirstOrDefault();
            if(userData != null)
            {
                return MapToATUser();
            }

            return null;
        }
        #endregion

        #region NotImplemented Methods
       
        public Task CreateAsync(Entities.Authentication.User user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Entities.Authentication.User user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(Entities.Authentication.User user)
        {
            throw new NotImplementedException();
        }

        public Task<Entities.Authentication.User> FindByIdAsync(long userId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetLockoutEnabledAsync(Entities.Authentication.User user)
        {
            return Task.FromResult(true);
        }

        public Task SetLockoutEnabledAsync(Entities.Authentication.User user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(Entities.Authentication.User user)
        {
            throw new NotImplementedException();
        }
        public Task SetLockoutEndDateAsync(Entities.Authentication.User user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }


        #endregion
    }

}