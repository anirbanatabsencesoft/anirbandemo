﻿using AT.Entities.Authentication;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AT.Data.Authentication
{
    /// <summary>
    /// The IATUserStore implements from IUserStore, IUserLockoutStore and represents the interface for the Authentication repository.
    /// </summary>
    public interface IUserStore : IUserLockoutStore<User,long>
    {
        /// <summary>
        /// Gets or sets the maximum failed access attempts before lockout.
        /// </summary>
        /// <value>
        /// The maximum failed access attempts before lockout.
        /// </value>
        int MaxFailedAccessAttemptsBeforeLockout { get; set; }
        /// <summary>
        /// Gets the customer secret key asynchronous.
        /// </summary>
        /// <param name="customerKey">The customer key.</param>
        /// <returns></returns>
        Task<string> GetCustomerSecretKeyAsync(string customerKey);
        /// <summary>
        /// Gets the roles and permissions.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        Task<List<Role>> GetRolesAndPermissions(List<string> roles);
        /// <summary>
        /// Finds the user by key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        Task<User> FindByKey(string key);
        /// <summary>
        /// Gets the lockout date asynchronous.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        Task<DateTime?> GetLockoutDateAsync(User user);
        /// <summary>
        /// Sets the lockout date asynchronous.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="lockoutDate">The lockout date.</param>
        /// <returns></returns>
        Task SetLockoutDateAsync(User user, DateTime lockoutDate);
        /// <summary>
        /// Unlock a user account asynchronous.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        Task UnLockAccountAsync(User user);

        Task<bool> ValidateIsPasswordExpired(User user);

        /// <summary>
        /// Method allows fetching the inactive logout period for the customerkey
        /// </summary>
        /// <param name="appType"></param>
        /// <param name="customerKey"></param>
        /// <returns></returns>
        Task<int> GetCustomerInactiveLogoutPeriodAsync(ApplicationType appType, string customerKey);
    }
}
