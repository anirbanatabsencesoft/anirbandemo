﻿using System.Collections.Specialized;

namespace AT.Data.Authentication.Provider
{
    /// <summary>
    /// The abstract base  provider class for User Store that uses IATUserStore interface as a repository. 
    /// It provides the base class to inherit by all the possible UserStore data access providers.
    /// </summary>
    /// <seealso cref="System.Configuration.Provider.ProviderBase" />
    public abstract class BaseUserStoreProvider : System.Configuration.Provider.ProviderBase
    {
        /// <summary>
        /// Sets the configuration.
        /// </summary>
        /// <param name="collection">The collection.</param>
        public abstract void SetConfig(NameValueCollection collection);

        /// <summary>
        /// This is to set in set config method using switch/factory
        /// </summary>
        public IUserStore Repository { get; set; }

    }
}
