using System.Configuration;
using System.Web.Configuration;

namespace AT.Data.Authentication.Provider
{
    /// <summary>
    /// This is the starting point for a UserStore provider as this is going to set
    /// all providers.
    /// </summary>
    public class UserStoreProviderManager
    {

        private static BaseUserStoreProvider _default;
        private static UserStoreProviderCollection _providerCollection;
        private static System.Configuration.ProviderSettingsCollection _providerSettings;


        /// <summary>
        /// Static constructor for getting all providers in a queue
        /// </summary>
        static UserStoreProviderManager()
        {

            Initialize();
        }

        /// <summary>
        /// Initialize the configuration section and set all static private variables.
        /// </summary>
        private static void Initialize()
        {
            //get the configuration
            UserStoreProviderConfiguration configSection = (UserStoreProviderConfiguration)ConfigurationManager.GetSection("UserStoreProviders");

            //throw exception if configuration section is not present
            if (configSection == null)
                throw new ConfigurationErrorsException("UserStore provider section is not set in the config.");

            //get provider collections
            _providerCollection = new UserStoreProviderCollection();
            ProvidersHelper.InstantiateProviders(configSection.Providers, _providerCollection, typeof(BaseUserStoreProvider));

            //get provider settings
            _providerSettings = configSection.Providers;

            //if there is no default provider, throw excpetion
            if (_providerCollection[configSection.DefaultProvider] == null)
                throw new ConfigurationErrorsException("Default provider is not set.");

            //get the default provider
            _default = _providerCollection[configSection.DefaultProvider];
            var defaultSettings = _providerSettings[configSection.DefaultProvider];


            _default.SetConfig(defaultSettings.Parameters);
        }

        /// <summary>
        /// Returns the Default Instance of the provider.
        /// </summary>
        public static BaseUserStoreProvider Default
        {
            get
            {
                return _default;
            }
        }

        public static UserStoreProviderCollection Providers
        {
            get
            {
                return _providerCollection;
            }
        }

        public System.Configuration.ProviderSettingsCollection ProviderSettings
        {
            get
            {
                return _providerSettings;
            }
        }

    }//end UserStoreProviderManager

}//end namespace UserStore