using System.Configuration.Provider;

namespace AT.Data.Authentication.Provider
{
    /// <summary>
    /// Represets a collection of BaseUserStoreProvider objects thats inhertis from ProviderCollection.
    /// </summary>
    /// <seealso cref="System.Configuration.Provider.ProviderCollection" />
    public class UserStoreProviderCollection : ProviderCollection
    {

        /// <summary>
        /// Returns the UserStore provider based on name
        /// </summary>
        /// <param name="name"></param>
        new public BaseUserStoreProvider this[string name]
        {
            get
            {
                return (BaseUserStoreProvider)base[name];
            }
        }

    }//end UserStoreProviderCollection

}//end namespace UserStore