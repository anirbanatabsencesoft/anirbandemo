using System.Configuration;

namespace AT.Data.Authentication.Provider
{
    /// <summary>
    /// Represents a user store provier section with in a configuration file.
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationSection" />
    public class UserStoreProviderConfiguration : System.Configuration.ConfigurationSection
    {

        /// <summary>
        /// Gets the providers.
        /// </summary>
        /// <value>
        /// The providers.
        /// </value>
        [ConfigurationProperty("providers")]
        public System.Configuration.ProviderSettingsCollection Providers
        {
            get
            {
                return (ProviderSettingsCollection)base["providers"];
            }
        }

        /// <summary>
        /// Gets the default provider thats set to UserMongoDataProvider.
        /// </summary>
        /// <value>
        /// The default provider.
        /// </value>
        [ConfigurationProperty("default", DefaultValue = "UserMongoDataProvider")]
        public string DefaultProvider
        {
            get
            {
                return base["default"] as string;
            }
        }

    }//end UserStoreProviderConfiguration

}//end namespace UserStore