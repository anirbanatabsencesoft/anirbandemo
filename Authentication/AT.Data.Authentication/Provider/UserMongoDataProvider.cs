﻿using System;
using System.Collections.Specialized;

namespace AT.Data.Authentication.Provider
{
    /// <summary>
    /// The concrete implementation for BaseUserStoreProvider for Mongo data provider.
    /// </summary>
    /// <seealso cref="AT.Data.Authentication.Provider.BaseUserStoreProvider" />
    public class UserMongoDataProvider : BaseUserStoreProvider
    {

        public UserMongoDataProvider() : base()
        {
        }

        /// <summary>
        /// Reads the configuration and sets the Repository to an object of MongoUserStoreRepository
        /// </summary>
        /// <param name="collection">The collection.</param>
        public override void SetConfig(NameValueCollection collection)
        {
            //set the DB Provider
            this.Repository = new MongoUserStoreRepository();
            this.Repository.MaxFailedAccessAttemptsBeforeLockout = collection["maxFailedAccessAttemptsBeforeLockout"] != null ? Convert.ToInt16(collection["maxFailedAccessAttemptsBeforeLockout"]) : 3;
        }
    }
}
