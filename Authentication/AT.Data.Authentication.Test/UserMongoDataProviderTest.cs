﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AT.Data.Authentication.Provider;
using AT.Data.Authentication;

namespace AT.Data.Authentication.Test
{
    [TestClass]
    public class UserMongoDataProviderTest
    {
        [TestMethod]
        public void CheckDefaultProvider()
        {
            BaseUserStoreProvider provider = UserStoreProviderManager.Default;
            Assert.IsInstanceOfType(provider, typeof(BaseUserStoreProvider));
            Assert.IsInstanceOfType(provider.Repository, typeof(MongoUserStoreRepository));
        }
    }
}
