﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using AT.Data.Authentication.Provider;
using AT.Entities.Authentication;
using Microsoft.Owin.Security;
using Moq;
using System.Web;
using System.Linq;
using AT.Data.Authentication;
using System.Collections.Generic;

namespace AT.Logic.Authentication.Test
{
    
    [TestClass]
    public class UserManagerTest
    {
        private List<User> TestDataUsers;
        
        public UserManagerTest()
        {
            TestDataUsers = new List<User>(ConfigureTestData.PopulateUser());
        }

        public UserManagerTest(List<User> testUserData)
        {
            TestDataUsers = testUserData;
        }

        [TestMethod]
        public async Task TestFindByName()
        {
            string userName = "dev@absencesoft.com";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.FindByNameAsync(It.IsAny<string>())).Returns(
                (string param) => Task.FromResult(TestDataUsers.Find(x => x.UserName == param))
                );
            User user = await userManager.FindByNameAsync(userName);
            Assert.IsNotNull(user, "User doesn't not exists!");
        }
        [TestMethod]
        public async Task TestFindByName_WithBadUserName()
        {
            string userName = "test@test.com";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.FindByNameAsync(It.IsAny<string>())).Returns(
                (string param) => Task.FromResult(TestDataUsers.Find(x => x.UserName == param))
                );
            User user = await userManager.FindByNameAsync(userName);
            Assert.IsNull(user, "TestFindByName_WithBadUserName failed, bad user name should be null!");
        }
        [TestMethod]
        public async Task TestFindByKey()
        {
            string key = "000000000000000000000001";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.FindByKey(It.IsAny<string>())).Returns(
                (string param) => Task.FromResult(TestDataUsers.Find(x => x.Key == param))
                );
            User user = await userManager.FindByKey(key);
            Assert.IsNotNull(user, "TestFindByKey failed! User doesn't not exists!");
        }
        [TestMethod]
        public async Task TestFindByKey_WithBadKey()
        {
            string key = "111111111111111111111111";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.FindByKey(It.IsAny<string>())).Returns(
                (string param) => Task.FromResult(TestDataUsers.Find(x => x.Key == param))
                );
            User user = await userManager.FindByKey(key);
            Assert.IsNull(user, "TestFindByKey_WithBadUserName failed! Invalid User should return null!");
        }
        [TestMethod]
        public async Task ValidateToken_ValidTokenTest()
        {
            AuthenticationServerTest appServer = new AuthenticationServerTest();
            string token=await appServer.GetAccessToken_ValidCredentials();
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.GetCustomerSecretKeyAsync(It.IsAny<string>())).Returns(
                (string audId) => 
                {
                    if (audId == null)
                    {
                        return Task.FromResult(Constants.ADMIN_SECRET_KEY);
                    }
                    else
                    {
                        return Task.FromResult("OTk4MTk1NzktZDdhZC00MmVlLTgzNjItZWU1YWE3NWE2MWZiMndyazJlbmUudHNh");
                    }
                });

            AuthenticationTicket result= await userManager.ValidateTokenAsync(token);
            // mocking HttpContext
            Assert.IsNotNull(result, "Invalid token! ValidateToken failed returning null");
            Assert.IsTrue(result.Identity.Claims.Count() > 0, "Invalid token! ValidateToken failed  with no claims.");            
        }
        [TestMethod]
        public async Task ValidateToken_InValidTokenTest()
        {

            string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6InRlc3QiLCJzdWIiOiJ0ZXN0Iiwicm9sZSI6WyJNYW5hZ2VyIiwiU3VwZXJ2aXNvciJdLCJpc3MiOiJodHRwOi8vand0YXV0aHpzcnYuYXp1cmV3ZWJzaXRlcy5uZXQiLCJhdWQiOiIwOTkxNTNjMjYyNTE0OWJjOGVjYjNlODVlMDNmMDAyMiIsImV4cCI6MTUwNTIxNTk3MCwibmJmIjoxNTA1MjE0MTcwfQ.i8f6p7x98ctPwoaabLt8PL7vsRkaRI-kem0B5ZGwvYY";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.GetCustomerSecretKeyAsync(It.IsAny<string>())).Returns(
                (string audId) =>
                {
                    if (audId == null)
                    {
                        return Task.FromResult(Constants.ADMIN_SECRET_KEY);
                    }
                    else
                    {
                        return Task.FromResult("OTk4MTk1NzktZDdhZC00MmVlLTgzNjItZWU1YWE3NWE2MWZiMndyazJlbmUudHNh");
                    }
                });
            AuthenticationTicket result = await userManager.ValidateTokenAsync(token);
            // mocking HttpContext
            Assert.IsNull(result, "Invalid token! ValidateToken failed returning null");
        }
        [TestMethod]
        public async Task SetClaimsPrincipalTest()
        {
            AuthenticationServerTest appServer = new AuthenticationServerTest();
            string token = await appServer.GetAccessToken_ValidCredentials();
            string userName = "dev@absencesoft.com";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.GetCustomerSecretKeyAsync(It.IsAny<string>())).Returns(
                (string audId) =>
                {
                    if (audId == null)
                    {
                        return Task.FromResult(Constants.ADMIN_SECRET_KEY);
                    }
                    else
                    {
                        return Task.FromResult("OTk4MTk1NzktZDdhZC00MmVlLTgzNjItZWU1YWE3NWE2MWZiMndyazJlbmUudHNh");
                    }
                });

            AuthenticationTicket authenticationTicket = await userManager.ValidateTokenAsync(token);

            var request = new Mock<HttpRequestBase>();            
            request.Setup(r => r.HttpMethod).Returns("GET");
            var mockHttpContext = new Mock<HttpContextBase>();
            mockHttpContext.Setup(c => c.Request).Returns(request.Object);            
           
            var userPrincipal=await userManager.SetClaimsPrincipal(authenticationTicket,mockHttpContext.Object);
            
            mockHttpContext.Setup(ctx => ctx.User).Returns(userPrincipal);
            var identityUser = mockHttpContext.Object.User;
            Assert.IsNotNull(identityUser, "setClaimsPrincipalTest failed! Principal Identity user couldn't be set.");
            Assert.AreEqual(userName, identityUser!=null?identityUser.Identity.Name:string.Empty, "setClaimsPrincipalTest failed! Username doesn't match");
        }
        [TestMethod]
        public async Task SetClaimsPrincipal_WithInValidTokenTest()
        {

            string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6InRlc3QiLCJzdWIiOiJ0ZXN0Iiwicm9sZSI6WyJNYW5hZ2VyIiwiU3VwZXJ2aXNvciJdLCJpc3MiOiJodHRwOi8vand0YXV0aHpzcnYuYXp1cmV3ZWJzaXRlcy5uZXQiLCJhdWQiOiIwOTkxNTNjMjYyNTE0OWJjOGVjYjNlODVlMDNmMDAyMiIsImV4cCI6MTUwNTIxNTk3MCwibmJmIjoxNTA1MjE0MTcwfQ.i8f6p7x98ctPwoaabLt8PL7vsRkaRI-kem0B5ZGwvYY";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.GetCustomerSecretKeyAsync(It.IsAny<string>())).Returns(
                (string audId) =>
                {
                    if (audId == null)
                    {
                        return Task.FromResult(Constants.ADMIN_SECRET_KEY);
                    }
                    else
                    {
                        return Task.FromResult("OTk4MTk1NzktZDdhZC00MmVlLTgzNjItZWU1YWE3NWE2MWZiMndyazJlbmUudHNh");
                    }
                });

            AuthenticationTicket authenticationTicket = await userManager.ValidateTokenAsync(token);

            var request = new Mock<HttpRequestBase>();
            request.Setup(r => r.HttpMethod).Returns("GET");
            var mockHttpContext = new Mock<HttpContextBase>();
            mockHttpContext.Setup(c => c.Request).Returns(request.Object);

            var userPrincipal = await userManager.SetClaimsPrincipal(authenticationTicket, mockHttpContext.Object);
            Assert.IsNull(userPrincipal, "SetClaimsPrincipal_WithInValidTokenTest failed! Principal Identity returned isn't invalid.");
            
        }
        /// <summary>
        /// This test method is provided as an example for an API validate the token. Its an combination of different test cases already present with the test class 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ValidateRequestAPITest()
        {
            AuthenticationServerTest appServer = new AuthenticationServerTest();
            string token = await appServer.GetAccessToken_ValidCredentials();
            string userName = "dev@absencesoft.com";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.GetCustomerSecretKeyAsync(It.IsAny<string>())).Returns(
                (string audId) =>
                {
                    if (audId == null)
                    {
                        return Task.FromResult(Constants.ADMIN_SECRET_KEY);
                    }
                    else
                    {
                        return Task.FromResult("OTk4MTk1NzktZDdhZC00MmVlLTgzNjItZWU1YWE3NWE2MWZiMndyazJlbmUudHNh");
                    }
                });

            userStore.Setup(x => x.FindByNameAsync(It.IsAny<string>())).Returns(
              (string param) => Task.FromResult(TestDataUsers.Find(x => x.UserName == param))
              );

            AuthenticationTicket authenticationTicket = await userManager.ValidateTokenAsync(token);

            var request = new Mock<HttpRequestBase>();
            request.Setup(r => r.HttpMethod).Returns("GET");
            var mockHttpContext = new Mock<HttpContextBase>();
            mockHttpContext.Setup(c => c.Request).Returns(request.Object);

            var userPrincipal = await userManager.SetClaimsPrincipal(authenticationTicket, mockHttpContext.Object);

            mockHttpContext.Setup(ctx => ctx.User).Returns(userPrincipal);
            var identityUser = mockHttpContext.Object.User;
            Assert.IsNotNull(identityUser, "setClaimsPrincipalTest failed! Principal Identity user couldn't be set.");
            Assert.AreEqual(userName, identityUser != null ? identityUser.Identity.Name : string.Empty, "setClaimsPrincipalTest failed! Username doesn't match");
           
            //fetch the ATUser based on the IPricipal username
            User user = await userManager.FindByNameAsync(identityUser.Identity.Name);
            Assert.IsNotNull(user, "User doesn't not exists!");
        }
        [TestMethod]
        public async Task<int> UnLockAccountTest()
        {
            string userName = "dev@absencesoft.com";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.FindByNameAsync(It.IsAny<string>())).Returns(
           (string param) => Task.FromResult(TestDataUsers.Find(x => x.UserName == param))
           );
            userStore.Setup(x => x.UnLockAccountAsync(It.IsAny<User>())).Returns(
                (User testUser) =>
                {
                    testUser.FailedLoginAttempts = 0;
                    testUser.UserStatus = UserStatus.Active;
                    return Task.FromResult(testUser);
                }
               );
            userStore.Setup(x => x.MaxFailedAccessAttemptsBeforeLockout).Returns(3);
           User user = await userManager.FindByNameAsync(userName);
            await userManager.UnLockAccountAsync(user);            
            Assert.AreNotEqual(user.UserStatus,UserStatus.Locked, "UnLockAccount Failed.!");
            return userManager.MaxFailedAccessAttemptsBeforeLockout;
        }
        [TestMethod]
        public async Task<int> UnLockAccount_InValidKeyTest()
        {
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.FindByNameAsync(It.IsAny<string>())).Returns(
           (string param) => Task.FromResult(TestDataUsers.Find(x => x.UserName == param))
           );
            userStore.Setup(x => x.UnLockAccountAsync(It.IsAny<User>())).Returns(
               (User testUser) =>
               {
                   testUser.FailedLoginAttempts = 0;
                   testUser.UserStatus = UserStatus.Active;
                   return Task.FromResult(testUser);
               }
              );
            userStore.Setup(x => x.MaxFailedAccessAttemptsBeforeLockout).Returns(3);
            string userName = "dev@absencesoft.com";
            User user = await userManager.FindByNameAsync(userName);
            await userManager.UnLockAccountAsync(user);            
            Assert.IsNull(user, "UnLockAccount_InValidKeyTest Failed. Invalid key should return null!");
            return userManager.MaxFailedAccessAttemptsBeforeLockout;
        }
        [TestMethod]
        public async Task GetCustomerSecretKey_ValidTest()
        {
            string key = "000000000000000000000001";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.GetCustomerSecretKeyAsync(It.IsAny<string>())).Returns(
                (string audId) =>
                {
                    if (audId == null)
                    {
                        return Task.FromResult(Constants.ADMIN_SECRET_KEY);
                    }
                    else
                    {
                        return Task.FromResult("OTk4MTk1NzktZDdhZC00MmVlLTgzNjItZWU1YWE3NWE2MWZiMndyazJlbmUudHNh");
                    }
                });
            string secret=await userManager.GetCustomerSecretKey(key);           
            Assert.IsNotNull(secret, "GetCustomerSecretKey_ValidTest Failed. A valid key should return a value!");            
        }
        [TestMethod]
        public async Task GetCustomerSecretKey_InValidTest()
        {
            string key = "000000000000000000000000";
            var userStore = new Mock<IUserStore>();
            ApplicationUserManager userManager = new ApplicationUserManager(
                 userStore.Object);
            userStore.Setup(x => x.GetCustomerSecretKeyAsync(It.IsAny<string>())).Returns(
                (string audId) =>
                {
                    string result = null;                   
                    return Task.FromResult(result);                   
                });
            string secret = await userManager.GetCustomerSecretKey(key);
            Assert.IsNull(secret, "GetCustomerSecretKey_InValidTest Failed. Invalid key should return null!");
        }

       
    }
}
