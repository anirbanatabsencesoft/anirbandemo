﻿using AT.Entities.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Authentication.Test
{
    public class ConfigureTestData
    {
        public static List<User> PopulateUser()
        {
             List<User> TestDataUsers=new List<User>();
            //User with customer and administration role.
            User user1 = new User();
            user1.CustomerKey = "000000000000000000000002";
            user1.Email = "dev@absencesoft.com";
            user1.FailedLoginAttempts = 1;
            user1.IsATAdmin = false;
            user1.JobTitle = null;
            user1.Key = "000000000000000000000001";
            user1.LastFailedLogin = new System.DateTime(2017, 9, 27);
            user1.Password = "0FB4B21AD8B923F1ECD9DC93388121DB8AA168BF";
            user1.UserKey = null;
            user1.UserName = "dev@absencesoft.com";
            user1.UserStatus = UserStatus.Active;
            user1.Employers.Add(new EmployerAccess()
            {
                ActiveFrom = null,
                ActiveTo = null,
                EmployerId = "000000000000000000000002",
                IsActive = true,
                Key = new System.Guid("{9c66bc7a-6dc9-4fde-8c0e-1243dec4ba86}")
            });
            user1.Roles.Add(new Role()
            {
                ApplicationType = ApplicationType.Administration,
                Id = 0,
                Key = "000000000000000000000000",
                Name = "System Administrator"
            });
            TestDataUsers.Add(user1);

            //System Administrator
            User user2 = new User();
            user2.CustomerKey = "000000000000000000000002";
            user2.Email = "dev@absencesoft.com";
            user2.FailedLoginAttempts = 0;
            user2.IsATAdmin = true;
            user2.JobTitle = null;
            user2.Key = "000000000000000000000001";
            user2.Password = "0FB4B21AD8B923F1ECD9DC93388121DB8AA168BF";
            user2.UserName = "dev@absencesoft.com";
            user2.UserStatus = UserStatus.Active;
            user2.Roles.Add(new Role()
            {
                ApplicationType = ApplicationType.Administration,
                Id = 0,
                Key = "000000000000000000000000",
                Name = "System Administrator"
            });
            TestDataUsers.Add(user2);

            return TestDataUsers;
        }
    }
}
    