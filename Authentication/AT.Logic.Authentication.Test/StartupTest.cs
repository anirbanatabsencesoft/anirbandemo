using AT.Data.Authentication;
using AT.Data.Authentication.Provider;
using AT.Logic.Authentication.Server;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Configuration;
using System.Web.Http;

namespace AT.Logic.Authentication.Test
{
    /// <summary>
    /// The Class Startup class is required for oAuth configuration and OWIN initialization as an API
    /// </summary>
    public partial class StartupTest
    {
        private IUserStore _store;
        public StartupTest(IUserStore store)
        {
            _store = store;
        }        

        /// <summary>
        /// Configures the OWIN oAuth authentication properties 
        /// </summary>
        /// <param name="app">The application.</param>
        public void ConfigureOAuth(IAppBuilder app)
        {
            var appSettingsValue = ConfigurationManager.AppSettings["TokenExpirationTimeInMinutes"];
            var tokenExpirationTime= 30; //default to 30 mins
            if (appSettingsValue != null)
            {
                int.TryParse(appSettingsValue, out tokenExpirationTime);
            };
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                //For Dev enviroment only (on production should be AllowInsecureHttp = false)
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth2/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(tokenExpirationTime),
                Provider = new OAuthJwtServerProvider(_store),
                AccessTokenFormat = new AuthenticationJwtTokenFormat("AbsenceSoft", _store) 
            };

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);

        }

    }
}