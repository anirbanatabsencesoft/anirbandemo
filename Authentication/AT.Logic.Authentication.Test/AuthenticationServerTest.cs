﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Owin.Testing;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using AT.Logic.Authentication.Server;
using AT.Data.Authentication.Provider;
using AT.Entities.Authentication;
using AT.Data.Authentication;
using Moq;
using System;

namespace AT.Logic.Authentication.Test
{
    [TestClass]
    public class AuthenticationServerTest
    {
        private List<User> testDataUsers;

        public AuthenticationServerTest()
        {
            testDataUsers = new List<User>(ConfigureTestData.PopulateUser());
        }
        [TestMethod]
        public async Task GetToken_ValidCredentialsTest()
        {
            var token =await GetAccessToken_ValidCredentials();
            token.Should().NotBeNullOrEmpty();
        }

        public async Task<string> GetAccessToken_ValidCredentials()
        {
            JObject responseContent;
            string password = "!Complex001";          
            //unlock
            UserManagerTest userManagerTest = new UserManagerTest(testDataUsers);
            await userManagerTest.UnLockAccountTest();
            //Configure test store
            var userStore = ConfigureStore();            
            StartupTest startup = new StartupTest(userStore);            
            //passing in the Authentication Model
            using (var server = TestServer.Create(startup.ConfigureOAuth))
            {
                var response = await server.CreateRequest("/oauth2/token").And(x => x.Headers.Add(Constants.REQUEST_HEADER_APPLICATIONTYPE, ApplicationType.Portal.ToString()))
                    .And(x => x.Content = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("username", "dev@absencesoft.com"),
                            new KeyValuePair<string, string>("password", password),
                            new KeyValuePair<string, string>("grant_type", "password"),
                        })).PostAsync();
                responseContent = JObject.Parse(await response.Content.ReadAsStringAsync());
                response.StatusCode.Should().Be(HttpStatusCode.OK);
            }
            return  responseContent["access_token"].Value<string>();
        }

        [TestMethod]
        public async Task GetToken_InvalidCredentialsTest()
        {
            string password = "test"; //wrongtest           

            //Configure test store
            var userStore = ConfigureStore();
            StartupTest startup = new StartupTest(userStore);
            //passing in the Authentication Model
            using (var server = TestServer.Create(startup.ConfigureOAuth))
            {
                var response = await server.CreateRequest("/oauth2/token").And(x => x.Content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", "dev@absencesoft.com"),
                    new KeyValuePair<string, string>("password", password),
                    new KeyValuePair<string, string>("grant_type", "password")
                })).PostAsync();
                var responseContent = JObject.Parse(await response.Content.ReadAsStringAsync());
                var result = responseContent["error"].Value<string>();
                response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
                result.Should().BeOneOf("invalid_grant", "invalid_grant_lockedout", "invalid_grant_disabled");
            }

        }
        [TestMethod]
        public async Task GetToken_WithoutGrantTypeTest()
        {
            string password = "test"; //wrongtest         

            //Configure test store
            var userStore = ConfigureStore();
            StartupTest startup = new StartupTest(userStore);
            ApplicationUserManager userManager = new ApplicationUserManager(userStore);
            //passing in the Authentication Model
            using (var server = TestServer.Create(startup.ConfigureOAuth))
            {
                var response = await server.CreateRequest("/oauth2/token").And(x => x.Content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", "dev@absencesoft.com"),
                    new KeyValuePair<string, string>("password", password)                   
                })).PostAsync();
                var responseContent = JObject.Parse(await response.Content.ReadAsStringAsync());
                var result = responseContent["error"].Value<string>();
                response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
                result.Should().Be("unsupported_grant_type");
            }

        }
        [TestMethod]
        public async Task GetToken_WithoutParametersTest()
        {
            //Configure test store
            var userStore = ConfigureStore();
            StartupTest startup = new StartupTest(userStore);
            //passing in the Authentication Model
            using (var server = TestServer.Create(startup.ConfigureOAuth))
            {
                List<KeyValuePair<string,string>> obj = new List<KeyValuePair<string, string>>();
                var response = await server.CreateRequest("/oauth2/token").And(x => x.Content = new FormUrlEncodedContent(obj)).PostAsync();
                var responseContent = JObject.Parse(await response.Content.ReadAsStringAsync());
                var result = responseContent["error"].Value<string>();
                response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
                result.Should().Be("unsupported_grant_type");
            }

        }

        [TestMethod]
        public async Task GetToken_LockedoutWithInValidAttemptsTest()
        {
            string password = "test"; //wrongtest
            string key = "000000000000000000000001";          
            //unlock
            UserManagerTest userManagerTest = new UserManagerTest(testDataUsers);
            var maxCount=await userManagerTest.UnLockAccountTest();

            var userStore = ConfigureStore();
            StartupTest startup = new StartupTest(userStore);
            ApplicationUserManager userManager = new ApplicationUserManager(
                userStore);
            //passing in the Authentication Model
            using (var server = TestServer.Create(startup.ConfigureOAuth))
            {
                for (var i = 1; i <= maxCount; i++) 
                {
                    var response = await server.CreateRequest("/oauth2/token").And(x => x.Content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("username", "dev@absencesoft.com"),
                        new KeyValuePair<string, string>("password", password),
                        new KeyValuePair<string, string>("grant_type", "password")
                    })).PostAsync();
                    var responseContent = JObject.Parse(await response.Content.ReadAsStringAsync());
                    var result = responseContent["error"].Value<string>();
                    response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
                    result.Should().BeOneOf("invalid_grant", "invalid_grant_lockedout", "invalid_grant_disabled");
                    var isLockedOut = await userManager.IsLockedOutAsync(key);
                    if (isLockedOut) //locked out on the third attempt
                        result.Should().Be("invalid_grant_lockedout");
                    else
                        result.Should().BeOneOf("invalid_grant", "invalid_grant_disabled");
                    if (result != "invalid_grant")
                        break;
                }
            }
            await userManagerTest.UnLockAccountTest();
        }

        private IUserStore ConfigureStore()
        {
            var userStore = new Mock<IUserStore>();
            userStore.Setup(x => x.FindByNameAsync(It.IsAny<string>())).Returns(
              (string param) => Task.FromResult(testDataUsers.Find(x => x.UserName == param))
              );
            userStore.Setup(x => x.ValidateIsPasswordExpired(It.IsAny<User>())).Returns(Task.FromResult<bool>(false));
            userStore.Setup(x => x.MaxFailedAccessAttemptsBeforeLockout).Returns(3);
            userStore.Setup(x => x.SetLockoutDateAsync(It.IsAny<User>(), It.IsAny<DateTime>())).Returns(
                (User user, DateTime lockoutDate) =>
                {
                    return Task.FromResult(user.FailedLoginAttempts = 0);
                }
                );
            userStore.Setup(x => x.IncrementAccessFailedCountAsync(It.IsAny<User>())).Returns(
                (User user) =>
                {
                    user.LastFailedLogin = DateTime.Now;
                    return Task.FromResult(++user.FailedLoginAttempts);
                }
                );
            userStore.Setup(x => x.GetLockoutDateAsync(It.IsAny<User>())).Returns(
                (User user) =>
                {
                    DateTime? result = null;
                   if (user.FailedLoginAttempts ==0)
                    {
                        result = user.LastFailedLogin;
                        return Task.FromResult(result);
                    }
                   else
                    {
                        return Task.FromResult(result);
                    }
                }
                );
            userStore.Setup(x => x.UnLockAccountAsync(It.IsAny<User>())).Returns(
                (User testUser) =>
                {
                    testUser.FailedLoginAttempts = 0;
                    testUser.UserStatus = UserStatus.Active;
                    return Task.FromResult(testUser);
                }
               );
            userStore.Setup(x => x.GetCustomerSecretKeyAsync(It.IsAny<string>())).Returns(
                (string audId) =>
                {
                    if (audId == null)
                    {
                        return Task.FromResult(Constants.ADMIN_SECRET_KEY);
                    }
                    else
                    {
                        return Task.FromResult("OTk4MTk1NzktZDdhZC00MmVlLTgzNjItZWU1YWE3NWE2MWZiMndyazJlbmUudHNh");
                    }
                });
            userStore.Setup(x => x.GetCustomerInactiveLogoutPeriodAsync(It.IsAny<ApplicationType>(),It.IsAny<string>())).Returns(
               (ApplicationType appType, string custKey) =>
               {
                   if (appType == ApplicationType.Portal)
                   {
                       return Task.FromResult(190);
                   }
                   else if (appType == ApplicationType.SelfService)
                   {
                       return Task.FromResult(180);
                   }
                   else
                   {
                       return Task.FromResult(60);
                   }
               });
            return userStore.Object;
        }
    }
}
