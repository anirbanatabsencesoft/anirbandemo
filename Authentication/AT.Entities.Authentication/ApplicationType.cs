﻿namespace AT.Entities.Authentication
{
    /// <summary>
    /// Enum provides the values for the different kind of applications seeking Authentication.
    /// </summary>
    public enum ApplicationType
    {
        Portal = 0,
        SelfService = 1,
        Administration = 2
    }
  }
