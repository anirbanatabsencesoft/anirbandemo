namespace AT.Entities.Authentication
{
    /// <summary>
    /// The Enumeration UserStatus represent the current user status
    /// </summary>
    public enum UserStatus
    {

        Active = 1,
        Locked = 2,
        Disabled = 3,
        Deleted = 4

    }
}
