﻿namespace AT.Entities.Authentication
{
    public enum SignInStatus
    {
        Ok,
        Error,
        Locked,
        Disabled,
        Expired
    }
}
