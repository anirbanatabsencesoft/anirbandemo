using System.Collections.Generic;

namespace AT.Entities.Authentication
{
    /// <summary>
    /// The Class ATPermission represents the permissions similart to AbsenceSoft.Data.Security.Permissions.
    /// </summary>
    public class Permission
    {

        public Permission()
        {

        }

        public string Category { get; set; }

        public string HelpText { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Using the same pattern used in AT 1
        /// </summary>
        public static List<Permission> None { get { return new List<Permission>(); } }
        
    }

}
