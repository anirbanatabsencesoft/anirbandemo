using System.Collections.Generic;

namespace AT.Entities.Authentication
{
    /// <summary>
    /// The Class UserContext will represet the logged-in user in context of the caller application. 
    /// </summary>
    public class UserContext
    {

        public UserContext()
        {

        }

        public long? CustomerId { get; set; }

        public long? EmployeeId { get; set; }

        public long? EmployerId { get; set; }

        public List<Permission> Permissions { get; set; }

        public long UserId { get; set; }

        public string UserName { get; set; }

        public UserStatus Status { get; set; }

    }
}
