using System;
using System.Collections.Generic;

namespace AT.Entities.Authentication
{
    /// <summary>
    /// The Class ATEmployerAccess represents the Employer Access properties and their roles. 
    /// </summary>
    public class EmployerAccess
    {
        public EmployerAccess()
        {

        }
        public DateTime? ActiveFrom { get; set; }

        public DateTime? ActiveTo { get; set; }

        public List<string> ContactOf { get; set; }

        public string EmployerId { get; set; }
        public long Id { get; set; }
        /// <summary>
        /// Gets or sets the key.It represents the Id of the Absencesoft.Data.Security employer access.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public Guid Key { get; set; }

        public bool IsActive { get; set; }

        public List<Role> Roles { get; set; }

    }

}