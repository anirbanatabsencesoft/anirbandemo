using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using AbsenceSoft.Data.Security;
using System.Linq;

namespace AT.Entities.Authentication
{
    /// <summary>
    /// The Class ATRole implements IRole<string> interface that represents an identity Role
    /// </summary>
    public class Role : IRole<long>
    {
        public Role()
        {
          
        }

        /// <summary>
        /// Gets or sets the type of the application.
        /// </summary>
        /// <value>
        /// The type of the application.
        /// </value>
        public ApplicationType ApplicationType { get; set; }

        /// <summary>
        /// Summary: UserId for the user that is in the role
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Gets or sets the key.It represents the MongoDB ObjectId of a role the user has access to.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string Key { get; set; }

        /// <summary>
        /// Summary: RoleId for the role
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the permissions.It represents the list of permissions the role has access to.
        /// </summary>
        /// <value>
        /// The permissions.
        /// </value>
        public List<Permission> Permissions { get; set; }

        /// <summary>
        /// The system admin role
        /// </summary>
        public static Role SystemAdministrator
        {
            get
            {
                return new Role()
                {
                    Id = 0,
                    Key = "000000000000000000000000",
                    Name = "System Administrator",
                    Permissions = AbsenceSoft.Data.Security.Permission.All().Select(p => new Permission() { Id = p.Id }).ToList()
                };
            }
        }
    }
}
