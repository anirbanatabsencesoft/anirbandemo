using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.IdentityModel.Claims;

namespace AT.Entities.Authentication
{
    /// <summary>
    /// The Class ATUser implements Microsoft Identity IUser<String> interface and represent an Identity user.
    /// </summary>
    public class User : IUser<long>
    {

        public User()
        {
            Roles = new List<Role>();
            Claims = new List<Claim>();
            Employers = new List<EmployerAccess>();          
        }
        public long? CustomerId { get; set; }
        /// <summary>
        /// Gets or sets the customer key. It represents the MongoDB ObjectId of a Customer user belongs to. 
        /// </summary>
        /// <value>
        /// The customer key.
        /// </value>
        public string CustomerKey { get; set; }
        public long? EmployeeId { get; set; }
        /// <summary>
        /// Gets or sets the employee key.It represents the MongoDB ObjectId of an Employee.
        /// </summary>
        /// <value>
        /// The employee key.
        /// </value>
        public string EmployeeKey { get; set; }
        public long? EmployerId { get; set; }
        /// <summary>
        /// Gets or sets the employer key.It represents the MongoDB ObjectId of an Employer the user has access to.
        /// </summary>
        /// <value>
        /// The employer key.
        /// </value>
        public string EmployerKey { get; set; }
        public List<Claim> Claims { get; set; }

        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the employers.It represents the lists of employers a user has access to.
        /// </summary>
        /// <value>
        /// The employers.
        /// </value>
        public List<EmployerAccess> Employers { get; set; }

        /// <summary>
        /// Gets or sets the failed login attempts.
        /// </summary>
        /// <value>
        /// The failed login attempts.
        /// </value>
        public int FailedLoginAttempts { get; set; }

        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the key. It represents the mongo objectId
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string Key { get; set; }

        public string JobTitle { get; set; }

        /// <summary>
        /// Gets or sets the last failed login.
        /// </summary>
        /// <value>
        /// The last failed login.
        /// </value>
        public DateTime? LastFailedLogin { get; set; }

        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        /// <value>
        /// The roles.
        /// </value>
        public List<Role> Roles { get; set; }

        public string UserKey { get; set; }

        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the user status. User status of an user can be Active, Locked, Disabled and Deleted
        /// </summary>
        /// <value>
        /// The user status.
        /// </value>
        public UserStatus UserStatus { get; set; }

        public bool IsATAdmin { get; set; }
    }

}
