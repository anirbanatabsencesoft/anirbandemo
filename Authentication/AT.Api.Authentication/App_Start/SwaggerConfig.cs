using System.Web.Http;
using WebActivatorEx;
using AT.Api.Authentication;
using Swashbuckle.Application;
using AT.Api.Core;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace AT.Api.Authentication
{
    /// <summary>
    /// Authentication Api Swagger Config
    /// </summary>
    public static class SwaggerConfig
    {
        /// <summary>
        /// Registers this by calling BaseSwaggerConfig and then adding the document filter.
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;
            BaseSwaggerConfig.Register("Authentication", thisAssembly, addTokenToHeader: false, configure: c => c.DocumentFilter<AuthTokenOperation>());
        }
    }
}
