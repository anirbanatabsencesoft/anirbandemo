﻿using AT.Entities.Authentication;
using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;

namespace AT.Api.Authentication
{
    /// <summary>
    /// Token operation
    /// </summary>
    public class AuthTokenOperation : IDocumentFilter
    {
        /// <summary>
        /// Swagger documentation
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="schemaRegistry"></param>
        /// <param name="apiExplorer"></param>
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            swaggerDoc.paths.Add("/oauth2/token", new PathItem
            {
                post = new Operation
                {
                    tags = new List<string> { "Authentication" },
                    description= "OAuth 2.0 OWIN Token end point URL to generate the token using resource owner password credentials grant flow.",                    
                    consumes = new List<string>
                {
                    "application/x-www-form-urlencoded"
                },  
                    parameters = new List<Parameter> {
                    new Parameter
                    {
                        type = "enum",
                        name = "X-AT-APPLICATION",
                        @enum =new List<object>(Enum.GetNames(typeof(ApplicationType)).ToList()),
                         schema=new Schema()
                        {
                            type="string"                           
                        },
                        required = true,                       
                        description="Custom header to identify the caller application type - Portal (default), SelfService, Administration.",
                        @in = "header"
                    },
                    new Parameter
                    {
                        type = "string",
                        name = "grant_type",
                         @default="password",
                        schema=new Schema()
                        {
                            type="string"                           
                        },
                        required = true,                                                        
                        description="Resource owner password credentials grant flow.",
                        @in = "formData"
                    },
                    new Parameter
                    {
                        type = "string",
                        name = "username",
                        description="Email Id as username.",
                        required = false,
                        @in = "formData"
                    },
                    new Parameter
                    {
                        type = "string",
                        name = "password",
                        description="Password text",
                        required = false,
                        @in = "formData"
                    }
                }
                }
            });
        }
    }
}
