﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Authentication
{
    /// <summary>
    /// Handles both Sha1 and Sha256 hasher
    /// </summary>
    public static class PasswordHashManager
    {
        /// <summary>
        /// returns the hashed password for both encryption
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static HashedPassword HashPassword(string password)
        {
            var sha1 = new PasswordSha1Hasher();
            var sha256 = new PasswordSha256Hasher();

            return new HashedPassword()
            {
                Sha1Encrypted = sha1.HashPassword(password),
                Sha256Encrypted = sha256.HashPassword(password)
            };
        }

        /// <summary>
        /// Returns the verification result for hashed password
        /// </summary>
        /// <param name="hashedPassword"></param>
        /// <param name="providedPassword"></param>
        /// <returns></returns>
        public static PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            var sha1 = new PasswordSha1Hasher();
            var sha256 = new PasswordSha256Hasher();

            var enc = sha1.HashPassword(providedPassword);
            if(enc == hashedPassword)
            {
                return PasswordVerificationResult.Success;
            }
            else
            {
                enc = sha256.HashPassword(providedPassword);
                if (enc == hashedPassword)
                {
                    return PasswordVerificationResult.Success;
                }
            }
            return PasswordVerificationResult.Failed;
        }
    }

    /// <summary>
    /// Model for hashed password
    /// </summary>
    public class HashedPassword
    {
        /// <summary>
        /// Sha1 encrypted password
        /// </summary>
        public string Sha1Encrypted { get; set; }

        /// <summary>
        /// Sha256 encrypted password
        /// </summary>
        public string Sha256Encrypted { get; set; }
    }
}
