﻿using AT.Data.Authentication.Provider;
using AT.Entities.Authentication;
using AT.Logic.Authentication.Server;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;


namespace AT.Logic.Authentication
{
    /// <summary>
    /// The class is provided for the WebApps and API to signin using the standard cookies Authentication Type. This may not be used
    /// but is provided as a standard constructs if required.
    /// </summary>
    public class ApplicationSignInManager : SignInManager<User, long>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSignInManager"/> class.
        /// </summary>
        /// <param name="authenticationManager">The authentication manager.</param>
        public ApplicationSignInManager(IAuthenticationManager authenticationManager)
            :base(new ApplicationUserManager(UserStoreProviderManager.Default.Repository), authenticationManager)
        {
        }

        public new ApplicationUserManager UserManager
        {
            get {
                    return (ApplicationUserManager)base.UserManager;
                }
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

        /// <summary>
        /// Sign in the user in using the user name and password
        /// Calls and Authenticate the credentials with the Authentication API 
        /// Gets the token, signIn the user Identity with the claims
        /// Add the token with the claims for future use.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>       
        /// <param name="appType">ApplicationType Portal = 0, SelfService = 1, Administration = 2</param>   
        /// <returns></returns>
        public async virtual Task<Entities.Authentication.SignInStatus> SignInAsync(string userName, string password, ApplicationType appType)
        {          
            var tokenResult = await GetTokenAsync(userName, password, appType);            
            if (tokenResult.Status == Entities.Authentication.SignInStatus.Ok)
            {               
                AuthenticationTicket authTicket = await UserManager.ValidateTokenAsync(tokenResult.Token);
                authTicket.Properties.IsPersistent = true;
                var claims = new List<Claim>(authTicket.Identity.Claims);
                claims.Add(new Claim("token", tokenResult.Token));
                var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);              
                AuthenticationManager.SignIn(authTicket.Properties, identity);
            }
            return await Task.FromResult(tokenResult.Status);
        }
        /// <summary>
        /// This method allows signing-in the user to AT2.0 WebApps during a SSO request from the customer portal
        /// The method is called after a SAML request is successfully authenticated by IDP and a successful SAML response is being processed.  
        /// Gets the token, signIn the user Identity with the claims
        /// Add the token with the claims for future use.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>True is sign-in is successful otherwise false if there is an error.</returns>
        public async virtual Task<bool> SignInSSOAsync(string userName, ApplicationType appType)
        {
            OAuthJwtServerProvider provider = new OAuthJwtServerProvider(UserManager.Store);
            AuthenticationJwtTokenFormat jwtToken = new AuthenticationJwtTokenFormat("AbsenceSoft",UserManager.Store);
            var user=UserManager.FindByName(userName);
            var audienceTypePropertyValue = user.CustomerId?.ToString() ?? user.CustomerKey;
            var authenticationTicket = await provider.GenerateAuthenticationTicket(user, audienceTypePropertyValue, appType.ToString());
            string token = null;
            if (authenticationTicket != null)
            {
                authenticationTicket.Properties.IssuedUtc = new DateTimeOffset(DateTime.UtcNow);
                authenticationTicket.Properties.ExpiresUtc = new DateTimeOffset(DateTime.UtcNow + TimeSpan.FromMinutes(await GetTokenExirationTime(appType, audienceTypePropertyValue)));
                authenticationTicket.Properties.IsPersistent = true;
                token = jwtToken.Protect(authenticationTicket);
            }
            if (!string.IsNullOrWhiteSpace(token))
            {
                var claims = new List<Claim>(authenticationTicket.Identity.Claims);
                claims.Add(new Claim("token", token));
                var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignIn(authenticationTicket.Properties, identity);
                return await Task.FromResult(true);
            }
            return false;
        }

        /// <summary>
        /// Sign in the user back by renewing the expiration using the same identity
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public async virtual Task<bool> SignInRenewAysnc(string accessToken, ApplicationType appType)
        {
            AuthenticationJwtTokenFormat jwtToken = new AuthenticationJwtTokenFormat("AbsenceSoft", UserManager.Store);
            AuthenticationTicket authenticationTicket = await UserManager.ValidateTokenAsync(accessToken);            
            if (authenticationTicket != null)
            {
                string customerKey = string.Empty;                
                authenticationTicket.Properties.Dictionary.TryGetValue(Constants.AUDIENCE_PROPERTY_KEY, out customerKey);                
                authenticationTicket.Properties.IssuedUtc = new DateTimeOffset(DateTime.UtcNow);
                authenticationTicket.Properties.ExpiresUtc = new DateTimeOffset(DateTime.UtcNow + TimeSpan.FromMinutes(await GetTokenExirationTime(appType, customerKey)));
                authenticationTicket.Properties.IsPersistent = true;
                var token = jwtToken.Protect(authenticationTicket);                        
                if (!string.IsNullOrWhiteSpace(token))
                {
                    authenticationTicket.Identity.TryRemoveClaim(authenticationTicket.Identity.FindFirst("token"));
                    var claims = new List<Claim>(authenticationTicket.Identity.Claims);
                    claims.Add(new Claim("token", token));                    
                    var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignIn(authenticationTicket.Properties, identity);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Signs the user out asynchronous.
        /// </summary>
        /// <returns></returns>
        public async Task SignOutAsync()
        {
            AuthenticationManager.SignOut();
            await Task.FromResult(1);            
        }

        /// <summary>
        /// Gets the token asynchronously by using HttpClient to AT.Api.Authentication
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password</param>
        /// <param name="appType">Type of the application.</param>
        /// <returns></returns>
        private async Task<TokenResult> GetTokenAsync(string userName, string password, ApplicationType appType)
        {
            var authModel = new Dictionary<string, string>();
            authModel.Add("grant_type", "password");
            authModel.Add("username", userName);
            authModel.Add("password", password);
            FormUrlEncodedContent content = new FormUrlEncodedContent(authModel);
            HttpClient client;
            //The URL of the WEB API Service
            string url = ConfigurationManager.AppSettings["TokenUrl"];
            client = new HttpClient();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Add(Constants.REQUEST_HEADER_APPLICATIONTYPE, appType.ToString());
            HttpResponseMessage responseMessage = await client.PostAsync(url, content);
            var responseContent = JObject.Parse(await responseMessage.Content.ReadAsStringAsync());
            if (responseMessage.IsSuccessStatusCode)
            {
                return (new TokenResult()
                {
                    Token = responseContent["access_token"].Value<string>(),
                    Expiry = TimeSpan.FromSeconds(responseContent["expires_in"].Value<int>()),
                    Status = Entities.Authentication.SignInStatus.Ok
                });
            }
            else
            {
                return (new TokenResult()
                {
                    Status = GetLoginResult(responseContent["error"].Value<string>())
                });
            }

        }

        /// <summary>
        /// Maps the response of Authentication Api to SignInStatus enum.
        /// </summary>
        /// <param name="response">The response string returned from Api</param>
        /// <returns></returns>
        private Entities.Authentication.SignInStatus GetLoginResult(string response)
        {
            Entities.Authentication.SignInStatus loginResult;
            switch (response)
            {
                case Constants.INVALID_GRANT:
                    loginResult = Entities.Authentication.SignInStatus.Error;
                    break;
                case Constants.INVALID_GRANT_LOCKEDOUT:
                    loginResult = Entities.Authentication.SignInStatus.Locked;
                    break;
                case Constants.INVALID_GRANT_DISABLED:
                    loginResult = Entities.Authentication.SignInStatus.Disabled;
                    break;
                case Constants.INVALID_GRANT_EXPIRED:
                    loginResult = Entities.Authentication.SignInStatus.Expired;
                    break;
                default:
                    loginResult = Entities.Authentication.SignInStatus.Ok;
                    break;
            }
            return loginResult;
        }

        /// <summary>
        /// The class holds the properties for token and SignInStatus as a result of GetTokenAsync method.
        /// </summary>
        private class TokenResult
        {
            public string Token { get; set; }
            public TimeSpan Expiry { get; set; }
            public Entities.Authentication.SignInStatus Status { get; set; }
        }

        /// <summary>
        /// The method reads the Token Expiration from config or return default to 30 mins.
        /// </summary>
        /// <returns>time in minutes as int</returns>
        private async Task<int> GetTokenExirationTime(ApplicationType appType, string customerKey)
        {
            return  await UserManager.GetCustomerInactiveLogoutPeriodAsync(appType, customerKey);
        }
    }
}
