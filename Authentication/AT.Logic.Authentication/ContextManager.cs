using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using AT.Entities.Authentication;

namespace AT.Logic.Authentication
{
    /// <summary>
    /// The Class ContextManager will be used to represent the current user context at caller's end at a given point of time, similar to Current.User
    /// </summary>
    public class ContextManager
    {

        public ContextManager()
        {
            Current = new UserContext();
        }

        public UserContext Current { get; set; }
    }
}
