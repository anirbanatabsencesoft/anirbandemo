using System;
using System.Collections.Generic;
using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Owin.Security;
using AT.Entities.Authentication;
using System.Linq;
using AT.Data.Authentication.Provider;
using AT.Data.Authentication;
using AT.Common.Log;
using System.IO;
namespace AT.Logic.Authentication.Server
{
    /// <summary>
    /// This class inherits from
    /// Microsoft.Owin.Security.OAuth.OAuthAuthorizationServerProvider and overrides GrantResourceOwnerCredentials methods
    /// OWIN Authorization Server implementation for grant_type=password, resource owner credentials flow.
    /// </summary>
    public class OAuthJwtServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly IUserStore _store = null;
        public OAuthJwtServerProvider(IUserStore store)
        {
            _store = store;
        }

        /// <summary>
        /// Grants the resource owner credentials.
        /// </summary>
        /// <param name="Context">The context.</param>
        /// <returns></returns>
        public async override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext Context)
        {
            ApplicationType appType=ApplicationType.Portal;
            Context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            // check for request header with the application type
            if (Context.Request.Headers[Constants.REQUEST_HEADER_APPLICATIONTYPE] != null)
            {
                Enum.TryParse<ApplicationType>(Context.Request.Headers[Constants.REQUEST_HEADER_APPLICATIONTYPE].ToString(), out appType);
            }
            ApplicationUserManager userManager = new ApplicationUserManager(_store);
            User user = null;
            try
            {
                if (Context.UserName != null)
                {
                    user = await userManager.FindByNameAsync(Context.UserName);
                    var tokenExpirationTime = await userManager.GetCustomerInactiveLogoutPeriodAsync(appType, user.CustomerKey);
                    Context.Options.AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(tokenExpirationTime);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                user = null;
            }
    
            string adminTypeAuthPropertyValue=null;
            string audienceTypePropertyValue = null;
            if (user != null && ValidateUserType(user,appType,out adminTypeAuthPropertyValue, out audienceTypePropertyValue) && !string.IsNullOrWhiteSpace(Context.Password))
            {
                if (user.UserStatus == UserStatus.Locked)
                {
                    Context.SetError(Constants.INVALID_GRANT_LOCKEDOUT, Constants.INVALID_GRANT_LOCKEDOUT_DESC);
                    return;
                }
                else if (user.UserStatus == UserStatus.Disabled)
                {
                    Context.SetError(Constants.INVALID_GRANT_DISABLED, Constants.INVALID_GRANT_DISABLED_DESC);
                    return;
                }
                else if (PasswordHashManager.VerifyHashedPassword(user.Password, Context.Password) != Microsoft.AspNet.Identity.PasswordVerificationResult.Success)
                {
                    //update the failed Login Attempt
                    var result = await userManager.AccessFailedAsync(user);
                    var errors = result.Errors.ToList();
                    Context.SetError(errors[0], errors[1]);
                    return;
                }
                else if (await userManager.ValidateIsPasswordExpired(user))
                {
                    Context.SetError(Constants.INVALID_GRANT_EXPIRED, Constants.INVALID_GRANT_EXPIRED_DESC);
                    return;
                }
            }
            else
            {
                Context.SetError(Constants.INVALID_GRANT, Constants.INVALID_GRANT_DESC);
                return;
            }

            //resetting the FailedAccessCount to zero 
            await userManager.ResetAccessFailedCountAsync(user.Key);
            var ticket = await GenerateAuthenticationTicket(user, audienceTypePropertyValue, adminTypeAuthPropertyValue);
            Context.Validated(ticket);
        }

        /// <summary>
        /// Validates the client authentication.
        /// </summary>
        /// <param name="Context">The context.</param>
        /// <returns></returns>
        public async override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext Context)
        {
            //The client validation is skipped and internally checked via user-->Customer object itself. 
            await Task.FromResult(Context.Validated());
        }

        /// <summary>
        /// The methods generate the Authentication Ticket with claims and authentication properties. 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="audienceTypePropertyValue"></param>
        /// <param name="adminTypeAuthPropertyValue"></param>
        /// <returns>AuthenticationTicket</returns>
        public async Task<AuthenticationTicket> GenerateAuthenticationTicket(User user,string audienceTypePropertyValue=null, string adminTypeAuthPropertyValue =null)
        {
            var identity = new ClaimsIdentity("JWT");

            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
            if (user.Id > -1)
                identity.AddClaim(new Claim("Id", user.Id.ToString(), ClaimValueTypes.Integer64));

            identity.AddClaim(new Claim("Key", user.Key));

            user.Roles.ToList().ForEach(p =>
            identity.AddClaim(new Claim(ClaimTypes.Role, p.Name))
            );
            var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {Constants.AUDIENCE_PROPERTY_KEY, audienceTypePropertyValue},
                    {Constants.APPLICATION_TYPE_PROPERTY_KEY,adminTypeAuthPropertyValue }
                });

            var ticket = new AuthenticationTicket(identity, props);
            return await Task.FromResult(ticket);
        }

        /// <summary>
        /// Validates the type of the user.
        /// If the application type is Administration then user should be of admin type Or
        /// If the application type is Portal / ESS then user should have a valid customer Id 
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="appType">Type of the application.</param>
        /// <param name="adminTypeAuthProperty">The admin type authentication property.</param>
        /// <returns></returns>
        private bool ValidateUserType(User user, ApplicationType appType, out string adminTypeAuthProperty, out string audienceTypePropertyValue)
        {
            if (user.IsATAdmin)
                adminTypeAuthProperty = ApplicationType.Administration.ToString();
            else
                adminTypeAuthProperty = null;

            audienceTypePropertyValue = null;
            if (!user.IsATAdmin)
            {
                audienceTypePropertyValue = user.CustomerId?.ToString() ?? user.CustomerKey;
            }

            return ((appType == ApplicationType.Administration && user.IsATAdmin) ||
                (appType != ApplicationType.Administration && !string.IsNullOrWhiteSpace(audienceTypePropertyValue)));
        }

    }
}