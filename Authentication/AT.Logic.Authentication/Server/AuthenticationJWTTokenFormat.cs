using System;
using System.Collections.Generic;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Thinktecture.IdentityModel.Tokens;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Linq;
using AT.Data.Authentication.Provider;
using AT.Entities.Authentication;
using AT.Data.Authentication;
using AT.Common.Log;

namespace AT.Logic.Authentication.Server
{
    /// <summary>
    /// This class inherits from
    /// Microsoft.OWIN.Security.ISecureDataFormat<AuthenticationTicket>
    /// interface for generating own JWT token format
    /// </summary>
    public class AuthenticationJwtTokenFormat : ISecureDataFormat<AuthenticationTicket>
    {
       
        private readonly string _issuer;
        private readonly IUserStore _store = null;
        public AuthenticationJwtTokenFormat(string issuer, IUserStore store)
        {
            _issuer = issuer;
            _store = store;

        }

        /// <summary>
        /// Protect menthod generates the JWT token.
        /// Verifies the Customer
        /// Gets the customer's appSecret.
        /// Generates the token
        /// sign the token singh the customer's appsecrets
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">Argument Data can't be null.</exception>
        /// <exception cref="System.InvalidOperationException">AuthenticationTicket.Properties does not include audience</exception>
        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            string audienceId = null;
            if (data.Properties.Dictionary.ContainsKey(Constants.AUDIENCE_PROPERTY_KEY))
                audienceId = data.Properties.Dictionary[Constants.AUDIENCE_PROPERTY_KEY];

            string appType = null;
            if (data.Properties.Dictionary.ContainsKey(Constants.APPLICATION_TYPE_PROPERTY_KEY))
                appType = data.Properties.Dictionary[Constants.APPLICATION_TYPE_PROPERTY_KEY];
            if (string.IsNullOrWhiteSpace(audienceId) && string.IsNullOrWhiteSpace(appType))

            {
                throw new InvalidOperationException(Constants.INVALID_OPERATION_AUTHENTICATIONTICKET_EXCECPTION);
            }

            string secret;
            if (appType==ApplicationType.Administration.ToString() || IsSystemUser(data.Identity)) //system admin
            {
                secret = Constants.ADMIN_SECRET_KEY;
            }
            else
            {
                //customer store or cache for checking the apikey and apisecret
                ApplicationUserManager userManager = new ApplicationUserManager(_store);
                secret = userManager.GetCustomerSecretKey(audienceId).Result;
            }
           
            var keyByteArray = TextEncodings.Base64Url.Decode(secret);

            var signingKey = new HmacSigningCredentials(keyByteArray);

            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;
            
            var token = new JwtSecurityToken(_issuer, audienceId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);

            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);

            return jwt;
        }

        /// <summary>
        /// Unprotect validates the specified JWT token and returns Microsoft.Owin.Security.AuthenticationTicket
        /// </summary>
        /// <param name="protectedText">The protected text.</param>
        /// <returns>AuthenticationTicket</returns>
        /// <exception cref="System.Exception">There is an error while validating the token.</exception>
        public AuthenticationTicket Unprotect(string protectedText)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                TokenValidationParameters validator = new TokenValidationParameters();
                JwtSecurityToken token = new JwtSecurityToken(protectedText);
                if (token != null)
                {
                    var audienceId = token.Audiences.FirstOrDefault();                   
                    //customer store or cache for checking the apikey and apisecret    
                    ApplicationUserManager userManager = new ApplicationUserManager(_store);                    
                    string secret;
                    if (string.IsNullOrWhiteSpace(audienceId)) // definetly Admin to get the token
                    {
                        secret = Constants.ADMIN_SECRET_KEY;
                        validator.ValidateAudience = false;
                    }
                    else
                    {
                        secret = userManager.GetCustomerSecretKey(audienceId).Result;
                    }
                    var keyByteArray = TextEncodings.Base64Url.Decode(secret);
                    var signingKey = new HmacSigningCredentials(keyByteArray);
                    validator.IssuerSigningKey = signingKey.SigningKey;
                    validator.ValidAudience = audienceId;
                    validator.ValidateLifetime = true;
                    validator.ValidIssuer = _issuer;
                    SecurityToken validatedToken = null;

                    ClaimsPrincipal claimsPrincipal = handler.ValidateToken(protectedText, validator, out validatedToken);
                    var props = new AuthenticationProperties();
                    if (string.IsNullOrWhiteSpace(audienceId)) // definetly Admin to get the token
                    {
                        props.Dictionary.Add(Constants.APPLICATION_TYPE_PROPERTY_KEY, ApplicationType.Administration.ToString());
                    }
                    else
                    {
                        props.Dictionary.Add(Constants.AUDIENCE_PROPERTY_KEY, audienceId);
                    }
                    props.IssuedUtc = new DateTimeOffset(token.ValidFrom); 
                    props.ExpiresUtc = new DateTimeOffset(token.ValidTo);
                    AuthenticationTicket ticket = new AuthenticationTicket((ClaimsIdentity)claimsPrincipal.Identity, props);
                    return ticket;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }           
        }

        private bool IsSystemUser(ClaimsIdentity identity)
        {
            return (identity.FindFirst("Key")?.Value == Constants.SYSTEM_USER_ID);
        }

    }
}