﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Logic.Authentication
{
    public static class Constants
    {
        /// <summary>
        /// The custom AT request header applicationtype
        /// </summary>
        public const string REQUEST_HEADER_APPLICATIONTYPE = "X-AT-APPLICATION";
        public const string INVALID_GRANT_LOCKEDOUT = "invalid_grant_lockedout";
        public const string INVALID_GRANT = "invalid_grant";
        public const string INVALID_GRANT_DISABLED = "invalid_grant_disabled";
        public const string INVALID_GRANT_EXPIRED = "invalid_grant_expired";
        public const string INVALID_GRANT_LOCKEDOUT_DESC = "The user is locked out.";
        public const string INVALID_GRANT_DESC = "The user name or password is incorrect.";
        public const string INVALID_GRANT_DISABLED_DESC = "The user is disabled.";
        public const string INVALID_GRANT_EXPIRED_DESC = "The password is expired.";
        public const string AUDIENCE_PROPERTY_KEY = "customer";
        public const string APPLICATION_TYPE_PROPERTY_KEY = "applicationType";
        public const string ADMIN_SECRET_KEY = "NjRhNzFmYjYtOGVkYi00OWNiLTg4OTEtZDE1MDU2ZWEwZmI2cXEzYXRnbnkuYmhx";
        public const string INVALID_OPERATION_AUTHENTICATIONTICKET_EXCECPTION = "AuthenticationTicket.Properties does not include audience.";
        public const string ARGUMENT_NULL_AUTHENTICATIONTICKET_DATA_EXCECPTION = "Argument Data can't be null.";
        public const string SYSTEM_USER_ID= "000000000000000000000000";
    }
}
