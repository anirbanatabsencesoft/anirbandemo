using Microsoft.AspNet.Identity;
using AT.Common.Core;

namespace AT.Logic.Authentication
{
    /// <summary>
    /// The Class PasswordSha1Hasher implements Microsoft Identity IPasswordHasher to implement the hashing algorithm present with AbsenceSoft.Data.Security
    /// </summary>
    public class PasswordSha1Hasher : IPasswordHasher
    {
        /// <summary>
        /// Hash a password
        /// </summary>
        /// <param name="password"></param>
        /// <returns>string</returns>
        public string HashPassword(string password)
        {
            return Utilities.ComputeSha1Hash(password);
        }

        /// <summary>
        /// Verify that a password matches the hashed password
        /// </summary>
        /// <param name="hashedPassword"></param>
        /// <param name="providedPassword"></param>
        /// <returns></returns>
        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {

            if (hashedPassword.Equals(providedPassword))
                return PasswordVerificationResult.Success;
            else return PasswordVerificationResult.Failed;
        }

    }
}
