﻿using AT.Data.Authentication;
using AT.Entities.Authentication;
using AT.Logic.Authentication.Server;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;

namespace AT.Logic.Authentication
{
    /// <summary>
    /// Configures the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    /// </summary>
    public class ApplicationUserManager : UserManager<User, long>
    {
        public ApplicationUserManager(IUserStore store)
            : base(store)
        {
            PasswordHasher = new PasswordSha256Hasher();
            Store = store;
            MaxFailedAccessAttemptsBeforeLockout = Store.MaxFailedAccessAttemptsBeforeLockout;
        }

        /// <summary>
        /// Gets the customer secret key.
        /// </summary>
        /// <param name="customerKey">The customer key.</param>
        /// <returns></returns>
        public async Task<string> GetCustomerSecretKey(string customerKey)
        {
            return await Store.GetCustomerSecretKeyAsync(customerKey);
        }

        /// <summary>
        /// Validates the token asynchronous.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>AuthenticationTicket that has a valid Claims Identity and Authentication properties.</returns>
        public async Task<AuthenticationTicket> ValidateTokenAsync(string token)
        {
            return await Task.FromResult<AuthenticationTicket>((new AuthenticationJwtTokenFormat("AbsenceSoft",Store)).Unprotect(token));
        }

        /// <summary>
        /// Finds the user by key, the mongoDB user's Object Id
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public async Task<User> FindByKey(string key)
        {
            return await Store.FindByKey(key);
        }

        /// <summary>
        /// Get the Identity principal from the token
        /// </summary>
        /// <param name="authenticationTicket"></param>
        /// <returns></returns>
        public async Task<IPrincipal> GetClaimsPrincipal(AuthenticationTicket authenticationTicket)
        {
            if (authenticationTicket != null)
            {
                var roles = authenticationTicket.Identity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(p => p.Value).ToArray<string>();
                GenericPrincipal myPrincipal = new GenericPrincipal(authenticationTicket.Identity, roles);
                IPrincipal identity = (IPrincipal)myPrincipal;
                return await Task.FromResult(identity);
            }
            else
                return null;
        }

        /// <summary>
        /// The method allows setting the Identity Prinicpal to the HttpContextBase.User object and return IPrincipal for testing.
        /// </summary>
        /// <param name="authenticationTicket"></param>
        /// <param name="httpContextBase"></param>
        /// <returns>IPrincipal</returns>
        public async Task<IPrincipal> SetClaimsPrincipal(AuthenticationTicket authenticationTicket, HttpContextBase httpContextBase)
        {
            IPrincipal identity = await GetClaimsPrincipal(authenticationTicket);
            if (identity != null && httpContextBase != null)
                httpContextBase.User = identity;
                
            return identity;
        }

        /// <summary>
        /// The method allows a user to login via system without password credentials.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="appType"></param>
        /// <returns>Authenticatoin ticket</returns>
        public async virtual Task<AuthenticationTicket> GetSystemTicketAsync(string userName, ApplicationType appType)
        {
            OAuthJwtServerProvider provider = new OAuthJwtServerProvider(Store);
            AuthenticationJwtTokenFormat jwtToken = new AuthenticationJwtTokenFormat("AbsenceSoft", Store);
            var user = await FindByNameAsync(userName);
            if (user != null)
            {
                string audienceTypePropertyValue = null;
                if (user.Key != Constants.SYSTEM_USER_ID) // if not a system user then use the customer key instead
                {
                    audienceTypePropertyValue = user.CustomerId?.ToString() ?? user.CustomerKey;
                }
                var authenticationTicket = await provider.GenerateAuthenticationTicket(user, audienceTypePropertyValue, appType.ToString());
                string token = null;
                if (authenticationTicket != null)
                {
                    authenticationTicket.Properties.IssuedUtc = new DateTimeOffset(DateTime.UtcNow);
                    authenticationTicket.Properties.ExpiresUtc = new DateTimeOffset(DateTime.UtcNow + TimeSpan.FromMinutes(await GetCustomerInactiveLogoutPeriodAsync(appType, audienceTypePropertyValue)));
                    authenticationTicket.Properties.IsPersistent = true;
                    token = jwtToken.Protect(authenticationTicket);
                }
                if (!string.IsNullOrWhiteSpace(token))
                {
                    authenticationTicket.Identity.AddClaim(new Claim("token", token));
                    return authenticationTicket;
                }
            }
            return null;
        }
        public new IUserStore Store { get; set; }

        /// <summary>
        /// Determines whether [is locked out asynchronous] [the specified key].
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public async Task<bool> IsLockedOutAsync(string key)
        {
            var user = await FindByKey(key);
            var lockedDate = await Store.GetLockoutDateAsync(user);
            return (lockedDate != null);
        }
                
        /// <summary>
        /// Increments the access failed count for the user and if the failed access account
        /// is greater than or equal to the MaxFailedAccessAttempsBeforeLockout, the user
        /// will be locked out for the next DefaultAccountLockoutTimeSpan and the AccessFailedCount
        /// will be reset to 0. This is used for locking out the user account.
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public async Task<IdentityResult> AccessFailedAsync(User user)
        {            
            var failedCount = ++user.FailedLoginAttempts;
            if (failedCount >= this.MaxFailedAccessAttemptsBeforeLockout)
            {
                //lockout the account                
                await Store.SetLockoutDateAsync(user, DateTime.Now);
                user.UserStatus = UserStatus.Locked;
                return IdentityResult.Failed("invalid_grant_lockedout", "The user login is locked out!");
            }
            else
            {
                await Store.IncrementAccessFailedCountAsync(user);
                return IdentityResult.Failed("invalid_grant","The user name or password is incorrect.");
            }
        }

        /// <summary>
        /// Unlocks the user account asynchronous.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public async Task UnLockAccountAsync(User user)
        {            
            await Store.UnLockAccountAsync(user);
        }

        /// <summary>
        /// Validates password expiry.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>True is expired otherwise false</returns>
        public async Task<bool> ValidateIsPasswordExpired(User user)
        {            
            return await Store.ValidateIsPasswordExpired(user);
        }
        
        /// <summary>
        /// Method find the inactive logout period based on the user's customer's security settings or default to config settings.
        /// </summary>
        /// <param name="appType"></param>
        /// <param name="customerKey"></param>
        /// <returns></returns>
        public async Task<int> GetCustomerInactiveLogoutPeriodAsync(ApplicationType appType,string customerKey)
        {
            int period = 0;
            if (!string.IsNullOrWhiteSpace(customerKey))
            {
                period = await Store.GetCustomerInactiveLogoutPeriodAsync(appType, customerKey);
            }
            if (period == 0)
            {
                period = GetDefaultInactiveConfigSettings(appType);
            }
            return period;
        }

        /// <summary>
        /// function allows fetching the config setting values or default to 60 mins
        /// </summary>
        /// <param name="appType"></param>
        /// <returns></returns>
        private int GetDefaultInactiveConfigSettings(ApplicationType appType)
        {
            string settingKey = "TokenExpirationTimeInMinutes";

            if (appType==ApplicationType.Portal)
            {
                settingKey = "InactiveLogoutPeriod";
            }
            else if (appType==ApplicationType.SelfService)
            {
                settingKey = "SelfServiceInactiveLogoutPeriod";
            }
            var appSettingsValue = ConfigurationManager.AppSettings[settingKey];
            var tokenExpirationTime = 60; //default to 60 mins
            if (appSettingsValue != null)
            {
                int.TryParse(appSettingsValue, out tokenExpirationTime);
            }
            else
            {
                appSettingsValue = ConfigurationManager.AppSettings["TokenExpirationTimeInMinutes"];
                if (appSettingsValue != null )
                {
                    int.TryParse(appSettingsValue, out tokenExpirationTime);
                }               
            }
            return tokenExpirationTime;
        }

        /// <summary>
        /// The method resets the LoginFailedAttempts count to zero.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task ResetAccessFailedCountAsync(string key)
        {
            User user =await FindByKey(key);
            await Store.ResetAccessFailedCountAsync(user);
        }
    }
}
