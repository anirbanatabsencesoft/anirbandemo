﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Npgsql;
using Insight.Database;
using AbsenceSoft.Data.Processing;
using System.Linq;
using AbsenceSoft.Logic.Processing.EL;
using AbsenceSoft.Data;
using AbsenceSoft.Logic.Administration;
using AbsenceSoft.Data.Customers;
using AbsenceSoft.Data.Enums;
using MongoDB.Driver;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace AbsenceSoft.Process.EL
{
    /// <summary>
    /// Process EL Dump data
    /// </summary>
    public class ElDataProcessor : IDisposable
    {
        #region Private variables

        string _pgConnectionString = "";
        int _batchSize = 1000;
        ElProcessingMaster _master = null;
        StringBuilder _message = new StringBuilder();

        EligibilityUpload _elu = null;
        List<CustomField> _employersCustomFields = null;
        List<EmployeeClass> _employeeClassTypes = null;
        Dictionary<string, ContactType> _contactTypes = null;
        Dictionary<string, string> _paySchedules = null;
        bool _captureErrMsg = false;
        bool _isCancelled = false;

        //set counter
        long _counter = 0;
        long _validRows = 0;
        long _invalidRows = 0;
        

        #endregion

        /// <summary>
        /// Constructor to inject values
        /// </summary>
        public ElDataProcessor()
        {
            _pgConnectionString = ConfigurationManager.ConnectionStrings["ELProcessingDatabase"].ConnectionString;
            Int32.TryParse(ConfigurationManager.AppSettings["BatchSize"], out _batchSize);
            
            //get the master
            GetElRecord();
        }

        #region Process

        /// <summary>
        /// Process records
        /// </summary>
        public void Process()
        {
            _captureErrMsg = ConfigurationManager.AppSettings["CaptureErrorMessage"].ToLowerInvariant() == "true";

            //process if there are records
            if(_master != null)
            {
                //start watch
                Console.WriteLine($"Started processing for Database Id: {_master.Id}, File: {_master.FileName}");
                Stopwatch watch = new Stopwatch();
                watch.Start();

                //get the eligibility upload
                _elu = EligibilityUpload.GetById(_master.ElUploadId);

                if (_elu != null)
                {
                    //get custom fields for employer
                    _employersCustomFields = GetEmployerCustomFields();
                    //Get Employee Employement types
                    _employeeClassTypes = EmployeeClass.DistinctFind(null, _master.CustomerId, _master.EmployerId).ToList();
                    //get all contact types
                    _contactTypes = GetEmployerContactTypes();

                    //get all pay schedules
                    _paySchedules = GetPaySchedules();

                    if (_master.ValidParsing > 0)
                    {
                        //loop through batch
                        while (_counter < _master.ValidParsing)
                        {
                            //get the latest eligibility upload if changed
                            _elu = EligibilityUpload.GetById(_master.ElUploadId);

                            var toBreak = ProcessRows();
                            if (toBreak == true)
                                break;
                        }                       
                    }

                    //update eligibility upload
                    if (_isCancelled)
                        _elu.Status = ProcessingStatus.Canceled;
                    else
                        _elu.Status = ProcessingStatus.Complete;
                    _elu.Save();

                    //delete temp table if set as true
                    try
                    {
                        bool toDeleteTempTable = ConfigurationManager.AppSettings["DeleteTempTableOnCompletion"].ToLowerInvariant() == "true";
                        if (toDeleteTempTable)
                            DropTempTable(_master.TempTable);
                    }
                    catch
                    {
                        _message.AppendLine($"Unable to delete temp table {_master.TempTable}");
                    }
                }
                else
                {
                    Console.WriteLine("No eligibility upload file identified.");
                }

                //update summary
                FinalizeSnapshot(_counter, _validRows, _invalidRows);

                watch.Stop();
                var ts = watch.Elapsed;
                Console.WriteLine($"Total time taken for the current EL Processing: {ts.Hours} hrs, {ts.Minutes} mins, {ts.Seconds} secs, {ts.Milliseconds} ms");
            }
            else
            {
                Console.WriteLine("No EL Record to process");
            }
        }

        /// <summary>
        /// Quickly does a lightweight parse of all well-known secondary employee numbers.
        /// </summary>
        /// <param name="rowParts">The row parts.</param>
        /// <returns></returns>
        private IEnumerable<string> QuickParseAllSecondaryEmployeeNumbers(string[] rowParts)
        {
            string Normalize(int index)
            {
                if (index >= rowParts.Length)
                    return null;
                string data = rowParts[index];
                if (string.IsNullOrWhiteSpace(data))
                    return null;
                data = Regex.Replace(data, "(^\")|(\"$)", "").Trim();
                data = data.Replace("\"\"", "\"");
                data = data.Replace("\\n", Environment.NewLine);
                if (string.IsNullOrWhiteSpace(data))
                    return null;
                return data;
            };

            // Ensure this is an actual record or record type and not a header row
            if (rowParts[0]?.Trim().Length != 1)
            {
                yield break;
            }

            //
            // Record Type
            var recType = Convert.ToChar(rowParts[0]);
            switch (recType)
            {
                case 'E':
                    yield return Normalize(32);
                    yield return Normalize(22);
                    yield return Normalize(27);
                    break;
                case 'S':
                case 'O':
                    yield return Normalize(2);
                    break;
                case 'B':
                    yield return Normalize(15);
                    break;
            }
        }


        private bool ProcessRows()
        {
            //Step 1: Get child records in batch
            var elList = GetChildRecords();

            //Step 2: Check if there are records
            if (elList != null && elList.Count > 0)
            {
                //Get Ids & employee numbers
                var ids = elList.Select(e => e.RowNumber).ToList();
                var empNos = elList.Select(e => e.EmployeeNumber)
                    .Union(elList.SelectMany(e => QuickParseAllSecondaryEmployeeNumbers(e.RowParts)).Where(e => !string.IsNullOrWhiteSpace(e)))
                    .ToList();

                //Step 3: Update status to processing i.e 1
                UpdateProcessingStatus(ids, 1);

                //Step 4: Create Argument
                var args = new BatchProcessingArguments()
                {
                    UploadedDoc = _elu,
                    CustomFields = _employersCustomFields,
                    EmployeeClassTypes = _employeeClassTypes,
                    Employees = GetEmployees(empNos),
                    Items = elList,
                    PaySchedules = _paySchedules,
                    ContactTypes = _contactTypes
                };

                //Step 5: Process
                ElBatchProcessingOutput elpOutput;
                using (var service = new EligibilityUploadBatchProcessingService())
                {
                    elpOutput = service.ProcessRecordsUsingBatch(args);
                }

                //increment counter
                _counter += elList.Count();

                //Step 6: Update status to processed i.e 2 and for failure 3

                var validIds = args.Items.Where(p => p.IsError == false).Select(p => p.RowNumber).ToList();
                UpdateProcessingStatus(validIds, 2);
                _validRows += validIds.Count;

                if (!_captureErrMsg)
                {
                    var errorIds = args.Items.Where(p => p.IsError == true).Select(p => p.RowNumber).ToList();
                    UpdateProcessingStatus(errorIds, 3);
                    _invalidRows += errorIds.Count();
                }
                else
                {
                    var errorRows = args.Items.Where(p => p.IsError == true).ToList();
                    UpdateProcessingStatus(errorRows, 3);
                    _invalidRows += errorRows.Count();
                }

                if (elpOutput.IsCancelled)
                {
                    _message.AppendLine("Processing is cancelled");
                    _isCancelled = true;
                    return true;
                }

                _message.AppendLine(elpOutput.ErrorMessage);
            }
            else
            {
                _message.AppendLine("All rows are processed.");
                return true;
            }

            return false;
        }


        /// <summary>
        /// Returns the custom fields for an employer
        /// </summary>
        /// <returns></returns>
        private List<CustomField> GetEmployerCustomFields()
        {
            return CustomField.DistinctFind(null, _master.CustomerId, _master.EmployerId, true)
                    .ToList().Where(cf => (cf.Target & EntityTarget.Employee) == EntityTarget.Employee).ToList();
        }

        /// <summary>
        /// Returns all contact types for the employer
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, ContactType> GetEmployerContactTypes()
        {
            var allContactTypes = new Dictionary<string, ContactType>();
            ContactType.AsQueryable()
                .Where(e => (e.CustomerId == null || e.CustomerId == _master.CustomerId) && (e.EmployerId == null || e.EmployerId == _master.EmployerId))
                .ToList()
                .OrderBy(a => !string.IsNullOrWhiteSpace(a.EmployerId) ? 0 : !string.IsNullOrWhiteSpace(a.CustomerId) ? 1 : 2)
                .ForEach(t =>
                {
                    if (allContactTypes.ContainsKey(t.Code))
                        return;
                    allContactTypes.Add(t.Code, t);
                });
            return allContactTypes;
        }

        /// <summary>
        /// Get the list of employees
        /// </summary>
        /// <param name="employeeNumbers"></param>
        /// <returns></returns>
        private List<Employee> GetEmployees(List<string> employeeNumbers)
        {
            IMongoQuery query = null;
            if(!string.IsNullOrWhiteSpace(_master.EmployerId))
                query = Employee.Query.And(Employee.Query.EQ(e => e.CustomerId, _master.CustomerId), Employee.Query.EQ(e => e.EmployerId, _master.EmployerId), Employee.Query.In(e => e.EmployeeNumber, employeeNumbers));
            else
                query = Employee.Query.And(Employee.Query.EQ(e => e.CustomerId, _master.CustomerId), Employee.Query.In(e => e.EmployeeNumber, employeeNumbers));

            return Employee.Repository.Collection.Find(query)
                                    .SetFlags(QueryFlags.NoCursorTimeout).ToList();
            
        }

        /// <summary>
        /// Returns the pay schedules
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetPaySchedules()
        {
            return PaySchedule.AsQueryable()
                     .Where(s => s.CustomerId == _master.CustomerId && s.EmployerId == _master.EmployerId && s.Name != null)
                     .ToDictionary(s => s.Name, s => s.Id);
        }

        /// <summary>
        /// Get child records
        /// </summary>
        private IList<EligibilityUploadResult> GetChildRecords()
        {
            IList<EligibilityUploadResult> result;
            var sql = $"SELECT row_id, employee_number, record_type_str, record_type_no, source_csv_string, values_arr, is_error, is_retry, error_message, processing_status FROM {_master.TempTable} WHERE processing_status < 2 ORDER BY record_type_no ASC, row_id ASC, processing_status DESC LIMIT {_batchSize} OFFSET 0;";
            using (var conn = new NpgsqlConnection(_pgConnectionString))
            {
                result = conn.QuerySql<EligibilityUploadResult>(sql);
            }

            if(result.Any())
            {
                result.ForEach(p => p.EligibilityUploadId = _elu.Id);
            }

            return result;
        }

        /// <summary>
        /// Update the status based on the action
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="status"></param>
        private void UpdateProcessingStatus(List<long> ids, int status)
        {
            var sql = $"UPDATE {_master.TempTable} SET processing_status={status} WHERE row_id IN (@ids)";
            using(var conn = new NpgsqlConnection(_pgConnectionString))
            {
                conn.ExecuteSql(sql, new { ids = ids});
            }
        }

        /// <summary>
        /// Overloaded method to set status with error message
        /// </summary>
        /// <param name="results"></param>
        /// <param name="status"></param>
        private void UpdateProcessingStatus(IList<EligibilityUploadResult> results, int status)
        {
            using (var conn = new NpgsqlConnection(_pgConnectionString))
            {
                conn.Open();
                foreach (var r in results)
                {
                    var sql = $"UPDATE {_master.TempTable} SET processing_status=@status, error_message=@message WHERE row_id=@id";
                    conn.ExecuteSql(sql, new { status = status, message = r.Error, id =r.RowNumber}, closeConnection:false);
                }
                conn.Close();
            }
        }

        /// <summary>
        /// Update the summary of el file processing
        /// </summary>
        /// <param name="totalRows"></param>
        /// <param name="validRows"></param>
        /// <param name="invalidRows"></param>
        private void FinalizeSnapshot(long totalRows, long validRows, long invalidRows)
        {
            var sql = "UPDATE public.el_file_processing SET is_processed = True, total_rows_processed=@processed, rows_passed_validation=@valid, rows_failed_validation=@invalid, processing_ended_on=public.utc(), error_message=@message, is_cancelled=@cancelled WHERE id=@id";
            using(var conn = new NpgsqlConnection(_pgConnectionString))
            {
                conn.ExecuteSql(sql, new { processed = totalRows, valid = validRows, invalid = invalidRows, message = _message.ToString(), cancelled = _isCancelled, id = _master.Id });
            }
        }


        /// <summary>
        /// Get the EL records to process
        /// If It was failed earlier with the same instance then that will be picked up first.
        /// Else a new one
        /// </summary>
        /// <returns></returns>
        private void GetElRecord()
        {
            var querySql = @"SELECT id, file_name, customer_id, employer_id, el_upload_id, parsed_row_temp_table_name, 
                               parse_started_on, parsed_completed_on, parse_invalid_rows, parse_valid_rows, 
                               parse_error_message, rows_failed_validation, rows_passed_validation, 
                               is_parse_complete, is_processed, processing_started_on, processing_ended_on, 
                               processed_by_server, is_error, error_message, total_rows_processed, 
                               total_rows_with_error
                         FROM public.el_file_processing 
                         WHERE is_parse_complete = True AND is_processed = False";

            //get data
            using(var conn = new NpgsqlConnection(_pgConnectionString))
            {
                _master = conn.SingleSql<ElProcessingMaster>($"{querySql} AND processed_by_server=text(inet_client_addr()) ORDER BY ID DESC LIMIT 1 OFFSET 0");
                if(_master == null)
                {
                    _master = conn.SingleSql<ElProcessingMaster>($"{querySql} AND processing_started_on IS NULL ORDER BY ID ASC LIMIT 1 OFFSET 0");
                    if(_master != null)
                    {
                        var updateSql = "UPDATE public.el_file_processing SET processed_by_server=text(inet_client_addr()), processing_started_on=public.utc() WHERE id=@id";
                        conn.ExecuteSql(updateSql, new { id= _master.Id});
                    }
                }
            }
        }

        /// <summary>
        /// Drop temp table
        /// </summary>
        /// <param name="tempTableName"></param>
        private void DropTempTable(string tempTableName)
        {
            using (var conn = new NpgsqlConnection(_pgConnectionString))
            {
                var updateSql = $" DROP TABLE {tempTableName};";
                conn.ExecuteSql(updateSql);
            }
        }
        #endregion

        /// <summary>
        /// Dispose off the objects
        /// </summary>
        public void Dispose()
        {
            _elu = null;
            _employersCustomFields = null;
            _contactTypes = null;
            _paySchedules = null;
            _pgConnectionString = null;
            _master = null;
            _message = null;
        }
    }
}
