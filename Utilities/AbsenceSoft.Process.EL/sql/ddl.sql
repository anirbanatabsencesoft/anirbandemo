﻿/* Add reference of the EL dump to temp schema and processing result */

/* Create Sequence */
CREATE SEQUENCE IF NOT EXISTS public.el_file_processing_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.el_file_processing_seq OWNER TO postgres;
GRANT ALL ON SEQUENCE public.el_file_processing_seq TO postgres;
GRANT SELECT, USAGE ON SEQUENCE public.el_file_processing_seq TO etldw;


/* Create a file processing table */
CREATE TABLE IF NOT EXISTS public.el_file_processing
(
	id bigint NOT NULL DEFAULT nextval('public.el_file_processing_seq'::regclass) PRIMARY KEY,
	file_name varchar(256) NOT NULL,
	customer_id varchar(32) NOT NULL,
	employer_id varchar(32),
	el_upload_id varchar(32),
	parsed_row_temp_table_name varchar(256) NOT NULL,
	parse_started_on timestamp with time zone DEFAULT public.utc(),
	parsed_completed_on timestamp with time zone,
	parse_invalid_rows bigint,
	parse_valid_rows bigint,
	parse_error_message text,
	rows_failed_validation bigint,
	rows_passed_validation bigint,
	is_parse_complete boolean NOT NULL default False,
	is_processed boolean NOT NULL default False,
	processing_started_on timestamp with time zone,
	processing_ended_on timestamp with time zone,
	processed_by_server varchar(256),
	is_error boolean NOT NULL default False,
	error_message text,
	total_rows_processed bigint,
	total_rows_with_error bigint,
	error_message_for_rows_with_error text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.el_file_processing OWNER TO postgres;

GRANT ALL ON TABLE public.el_file_processing TO postgres;
GRANT SELECT ON TABLE public.el_file_processing TO absence_tracker;
GRANT ALL ON TABLE public.el_file_processing TO etldw;

--Add is_cancelled status
ALTER TABLE public.el_file_processing
	ADD COLUMN IF NOT EXISTS is_cancelled boolean NOT NULL default False;