﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace AbsenceSoft.Process.EL
{
    /// <summary>
    /// Class to handle email
    /// </summary>
    public class EmailHandler : IDisposable
    {
        /// <summary>
        /// AWS Access Id
        /// </summary>
        private string AwsAccessId { get { return ConfigurationManager.AppSettings["AWSAccessKey"]; } }

        /// <summary>
        /// AWS Secret Key
        /// </summary>
        private string AwsSecretKey { get { return ConfigurationManager.AppSettings["AWSSecretKey"]; } }

        /// <summary>
        /// AWS Region
        /// </summary>
        private string AwsRegion { get { return ConfigurationManager.AppSettings["AWSRegion"]; } }

        /// <summary>
        /// Send Email based on the configuration
        /// </summary>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public bool SendMail(string errMessage = "")
        {
            try
            {
                var config = new AmazonSimpleEmailServiceConfig
                {
                    RegionEndpoint = RegionEndpoint.GetBySystemName(AwsRegion)
                };

                using (var client = new AmazonSimpleEmailServiceClient(AwsAccessId, AwsSecretKey, config))
                {
                    client.SendEmailAsync(GetMailRequest(errMessage)).GetAwaiter().GetResult();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get Email Request
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private SendEmailRequest GetMailRequest(string message = "")
        {
            //get the to addresses
            var to = Array.ConvertAll(ConfigurationManager.AppSettings["RecipientList"].Split(';'), p => p.Trim()).ToList();

            return new SendEmailRequest()
            {
                Source = ConfigurationManager.AppSettings["SenderAddress"],
                Destination = new Destination
                {
                    ToAddresses = to
                },
                Message = new Message
                {
                    Subject = new Content(ConfigurationManager.AppSettings["FailureSubject"]),
                    Body = new Body
                    {
                        Html = new Content
                        {
                            Charset = "UTF-8",
                            Data = message
                        },
                        Text = new Content
                        {
                            Charset = "UTF-8",
                            Data = message
                        }
                    }
                }
            };
        }         


        /// <summary>
        /// Dispose the object
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
