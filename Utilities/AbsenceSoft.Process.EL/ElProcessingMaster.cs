﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AbsenceSoft.Process.EL
{
    /// <summary>
    /// Represets the DAO for EL records stored
    /// </summary>
    public class ElProcessingMaster
    {
#pragma warning disable 0649
        long? id;
        public long Id { get { return id.Value;} set { id = value; } }

        string file_name;
        public string FileName { get { return file_name; }}

        string customer_id;
        public string CustomerId { get { return customer_id; } }

        string employer_id;
        public string EmployerId { get { return employer_id; } }

        string el_upload_id;
        public string ElUploadId { get { return el_upload_id; } }

        string parsed_row_temp_table_name;
        public string TempTable { get { return parsed_row_temp_table_name; } }

        DateTime parse_started_on;
        public DateTime ParseStartedOn { get { return parse_started_on; } }

        DateTime parsed_completed_on;
        public DateTime ParseCompletedOn { get { return parsed_completed_on; } }

        long parse_invalid_rows;
        public long InvalidParsing { get { return parse_invalid_rows; } }

        long parse_valid_rows;
        public long ValidParsing { get { return parse_valid_rows; } }

        string parse_error_message;
        public string ParseErrorMessage { get { return parse_error_message; } }

        long? rows_failed_validation;
        public long? FailedValidation { get { return rows_failed_validation; } }

        long? rows_passed_validation;
        public long? PassedValidation { get { return rows_passed_validation; } }

        bool is_parse_complete;
        public bool IsParseComplete { get { return is_parse_complete; } }

        bool is_processed;
        public bool IsProcessed { get { return is_processed; } }

        DateTime? processing_started_on;
        public DateTime? ProcessingStartedOn { get { return processing_started_on; } }

        DateTime? processing_ended_on;
        public DateTime? ProcessingEndedOn { get { return processing_ended_on; } }

        string processed_by_server;
        public string ProcessedByServer { get { return processed_by_server; } }

        bool is_error;
        public bool IsError { get { return is_error; } }

        string error_message;
        public string ErrorMessage { get { return error_message; } }

        long? total_rows_processed;
        public long? TotalRowsProcessed { get { return total_rows_processed; } }

        long? total_rows_with_error;
        public long? TotalRowsWithError { get { return total_rows_with_error; } }
#pragma warning restore 0649

    }
}
