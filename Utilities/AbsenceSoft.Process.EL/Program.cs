﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AbsenceSoft.Process.EL
{
    class Program
    {
        static void Main(string[] args)
        {
            //counters
            int totalErrorRetries = 10;
            int errCounter = 0;

            //serializers
            AbsenceSoft.Common.Serializers.DateTimeSerializer.Register();

            while (true)
            {
                try
                {
                    ProcessELRows();
                }
                catch (Exception ex)
                {
                    NotifyOnError(ex.ToString());
                    errCounter += 1;
                    Thread.Sleep(1000);
                    if (errCounter >= totalErrorRetries)
                    {
                        Console.WriteLine("Too many errors, hence stopping.");
                        Console.Beep();
                        Environment.Exit(9);
                    }
                }
            }
        }

        /// <summary>
        /// Process EL Rows and Sleep for 5 seconds
        /// </summary>
        static void ProcessELRows()
        {
            using (var eld = new ElDataProcessor())
            {
                eld.Process();
            }

            //sleep for some time
            int timeOutSecs = 10;
            int.TryParse(ConfigurationManager.AppSettings["DelayBetweenBatch"], out timeOutSecs);
            Thread.Sleep(timeOutSecs * 1000);
        }


        #region Notification
        /// <summary>
        /// Notify on error
        /// </summary>
        /// <param name="errMessage"></param>
        private static void NotifyOnError(string errMessage = "")
        {
            try
            {
                //send mail
                using (var eh = new EmailHandler())
                {
                    var msg = $"Hi there,<br/>The EL Pocessing Service has encountered some error hence closing.<br/>The error details...<br/>{errMessage}";
                    eh.SendMail(msg);
                }
            }
            catch { }
        }


        #endregion
    }
}
